﻿# 集客优铺小程序

#### 介绍
{**以下是码云平台说明，您可以替换此简介**
码云是开源中国推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1.   所有页面对应的名称
    "pages/describe/describe",   描述
    "pages/layout/layout",    发布
    "pages/qiuzulist/qiuzulist",    求租列表
    "pages/qiuzu_detail/qiuzu_detail",   求租详情展示
    "pages/my_layout1/my_layout1",   我的发布求租列表
    "pages/my_layout/my_layout",     我的发布房源列表
    "pages/my/my",              我的
    "pages/my_favorites/my_favorites",  我的收藏
    "pages/my_message/my_message",    我的消息
    "pages/my_certification/my_certification",   我的个人认证


    "pages/layout_ershoufang_chuzu/layout_ershoufang_chuzu",   发布二手房出租
    "pages/layout_ershoufang_sale/layout_ershoufang_sale",   发布二手房出售
    "pages/layout_ershoufang/layout_ershoufang",   发布二手房


    "pages/layout_qiuzu/layout_qiuzu",  发布求租
    "pages/jkyp_class/jkyp_class",    分类
    "pages/jkyp_index/jkyp_index",   首页

"pages/layout_changfang/layout_changfang",    发布厂房
    "pages/layout_changfang_zhuanzu/layout_changfang_zhuanzu",  发布厂房转租
    "pages/layout_changfang_sale/layout_changfang_sale",    发布厂房出售
"pages/layout_changfang_chuzu/layout_changfang_chuzu",    发布厂房出租


    "pages/layout_xiezilou_chuzu/layout_xiezilou_chuzu",    发布写字楼出租
    "pages/layout_xiezilou/layout_xiezilou",    发布写字楼
    "pages/layout_xiezilou_zhuanzu/layout_xiezilou_zhuanzu",    发布写字楼转租
    "pages/layout_xiezilou_sale/layout_xiezilou_sale",          发布写字楼出售


    "pages/layout_shangpu_zhuanrang/layout_shangpu_zhuanrang",   发布商铺转租
    "pages/layout_shangpu_chuzu/layout_shangpu_chuzu",            发布商铺出租
	"pages/layout_shangpu_sale/layout_shangpu_sale",              发布商铺出售
    "pages/shangpu_sale_next/shangpu_sale_next",                   发布商铺出售下一页
    "pages/details/details",                                    房源详情
    "pages/zhenshi/zhenshi",                    这是承诺页面
    "pages/index/index",               授权页面
    "pages/logs/logs",                   未写内容
    "component/city-list/city-list",      城市组件
    "pages/youzhifangyuan/youzhifangyuan",      优质房源列表
    "pages/guize/guize",                          集客优铺服务规则
    "pages/layout_shangpu/layout_shangpu",         发布商铺
    "pages/upimg/upimg",                        发布页面上传的图片均跳转至这个页面
    "pages/shangpu_chuzu_next/shangpu_chuzu_next",          发布商铺出租下一页资料填写
    "pages/shangpu_zhuanrang_next/shangpu_zhuanrang_next",    发布商铺转让下一页资料填写      
    "pages/my_recent/my_recent"             我的最近浏览
2. 点击所有发布页面的上传图片均跳转至upimg页面
3. 

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)