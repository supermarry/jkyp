// pages/youzhifangyuan/youzhifangyuan.js
var ea = require('../../utils/each.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getoutlistUrl:app.data.url+'api/api/getoutlist',
    page:1,
    fangyuanid:0,
    quanju:false,
    shi:'',
    // 搜索结果数组
    fangyuan: [
      
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    if(options.fujin==1){
      this.setData({shi:wx.getStorageSync('city')});
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    var that = this;
    var fangyuan = wx.getStorageSync('fangyuan');
    that.setData({fangyuanid:fangyuan});
    var openid = wx.getStorageSync('openid');
    var city = that.data.shi;
    that.setData({ fangyuan:[]});
    that.getoutinfo(fangyuan, 1, city)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if(that.data.page>0){
      var page = that.data.page+1;
      var openid = wx.getStorageSync('openid');
      var fangyuan = that.data.fangyuanid;
      var city = that.data.shi;
      that.getoutinfo(fangyuan, page, city)
    }
  },
  getoutinfo: function (fangyuan, page, city){
    var that = this;
    var openid = wx.getStorageSync('openid');
    wx.request({
      url: that.data.getoutlistUrl,
      data: { openid: openid, page: page, fangyuan:fangyuan,shi:city},
      method:'post',
      success:function(res){
        var data = res.data;
        console.log(data);
        if(data.code==200){
          console.log(data.data);
          var fangyuan = that.data.fangyuan;
          ea.each(data.data,function(i,v){
            fangyuan.push(v)
          })
          that.setData({page:page})
          that.setData({ fangyuan: fangyuan})
          if(data.data.length<5){
            that.setData({page:0});
            wx.showToast({
              title: '没有更多数据了',
              icon:'none'
            })
          }
          that.setData({quanju:true})
          wx.hideLoading();
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //点击跳转
  tiaozhuan:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('id', id);
    wx.navigateTo({
      url: '/pages/details/details',
    })
  }
})