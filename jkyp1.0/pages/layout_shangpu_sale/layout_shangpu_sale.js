// pages/layout_shangpu_sale/layout_shangpu_sale.js
const app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getShangpuurl: app.data.url +'api/api/getShangpu',
    senduser: app.data.url + '/api/Classify/send',
    getxzltypeurl: app.data.url + 'api/newapi/gettype',
    addoutUrl: app.data.url + 'api/newapi/addout',
    changeouturl: app.data.url + 'api/newapi/changeout',
    imgs:[],  //上传的图片数组,将选择第一张作为图片展示
    first_img:'',
    youtu:false,    //默认为false,
    keyishangchuang:true,
    backimg:"http://jikeyoupu.cybwnt.com/backgro.jpg", //背景图图片url地址
    shangpuleixing:["购物百货中心","临街门店","写字楼配套","档口摊位","社区底商","其他"],  //商铺类型
    shangpuleixingid: [],  //商铺类型id
    index:"",    //商铺类型点击选择的第几项，通过index可以获取id
    xianshi:true,   //请选择控制 默认为true
    jingying:["经营中","空置中"], //经营状态数组
    jingyingid:[],
    index1:'',
    xianshi1:true,
    jingyinghangye: ["美食", "科技","娱乐"], //经营行业数组
    jingyinghangyeid: [],
    index2: '',
    xianshi2: true,
    // 具体数据
    louceng:'',
    mianji:'',
    miankuan:'',
    cengao:'',
    jinsheng:'',
    jiaotong1:'',
    jiaotong2:'',
    sale_price:'',
    peitao:{},
    shouyi:'',
    yourname:'',
    phone:'',
    // 男女状态
    items: [
      { name: '1', value: '男' },
      { name: '0', value: '女', checked: 'true' },
    ],
    shuju: { sex: '女'}
  },

  // 上传图片
  upload:function(){
    var imgs = this.data.shuju.imgs;
    if(imgs){
      wx.setStorageSync('imgs', imgs);
    }
    wx:wx.navigateTo({
      url: '../upimg/upimg',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })

  },
  // 选择商铺类型
  bindPickerChange(e) {
  //   console.log('picker发送选择改变，携带值为', e.detail.value)
    var shangpuleixingid = this.data.shangpuleixingid;
    var shuju = this.data.shuju;
    shuju.types = shangpuleixingid[e.detail.value];
    console.log(shuju);
    this.setData({ shuju: shuju});
    this.setData({
      index: e.detail.value,
      xianshi: false,
    })
  },
  bindPickerChange1(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value);
    var jingying = this.data.jingying;
    var shuju = this.data.shuju;
    if (e.detail.value==0){
      shuju.zhuangtai = 1;
    }else{
      shuju.zhuangtai = 0;
    }
    console.log(shuju);
    this.setData({ shuju: shuju})
    this.setData({
      index1: e.detail.value,
      xianshi1: false,
    })
  },
  bindPickerChange2(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index2: e.detail.value,
      xianshi2: false,
    })
  },
  // 选择先生或女士
  radioChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value);
    var shuju = this.data.shuju;
    var inde = e.detail.value;
    if(inde==0){
      shuju.sex='女';
    }else{
      shuju.sex = '男';
    }
    console.log(shuju);
    this.setData({ shuju: shuju})
  },
  // 获取页面所有input框中的值
  inputWacth: function (e) {
    //console.log(e)
    let item = e.currentTarget.dataset.model;
    var shuju = this.data.shuju;
    shuju[item] = e.detail.value;
    console.log(shuju);
    this.setData({ shuju: shuju})
    // console.log(item);
    this.setData({
      [item]: e.detail.value
    });
    // var shangchuadata = this.data.shangchuadata;
    // shangchuadata.louceng = e.detail.value;
    // console.log(shangchuadata);
    // this.setData({ shangchuadata: shangchuadata});
  },
  // 点击下一步
  tonext:function(){
    var shuju = this.data.shuju;
  //检测是否有数据未填
    var shu = ea.yanzhen(shuju, {
      imgs:'请填入图片',
      types:'请填入商铺类型',
      louceng:'请填入楼层',
      proportion:'请填入面积',
      width:'请填入面宽',
      height:'请填入层高',
      depth:'请填入进深',
      zhuangtai:'请填入经营状态',
      price:'请填入售价',
      expect_shouyi:'请填入预计收益',
      last_name:'请填入联系人姓',
      sex:'请填写性别',
      lxr_phone:'请填入联系手机'
      });
    if(shu){
      wx.showToast({
        title: shu,
        icon:'none'
      });
      return;
    }
    wx.setStorageSync('peitao', '');
    var peitao = this.data.peitao;
    wx.setStorageSync('peitao', peitao);
    console.log(peitao);
    console.log(shuju);
    wx.setStorageSync('shuju', shuju);
    // 将所有数据放在内存里面，跳转至下一页
    wx.navigateTo({
      url: '../shangpu_sale_next/shangpu_sale_next',
    })
    
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setStorageSync('imgs', null);
    var that = this;
    var tiaojian = {};
    tiaojian.openid = wx.getStorageSync('openid');
    tiaojian.pid = 2;
    if(options.id){
      tiaojian.id = options.id;
    }
    wx.request({
      url: that.data.getxzltypeurl,
      data:tiaojian,
      success:function(res){
        var data = res.data;
        if(data.code==200){
          console.log(data);
          var shangpuleixing =[];
          var shangpuleixingid = [];
          ea.each(data.data.type,function(i,v){
            shangpuleixing.push(v.name);
            shangpuleixingid.push(v.id);
          })
          that.setData({ shangpuleixing: shangpuleixing, shangpuleixingid: shangpuleixingid,peitao:data.data.peitao});
          if(data.data.data){
            var shuju = data.data.data;
            shuju.imgs = shuju.img;
            shuju.easy_traffic1 = shuju.easy_traffic[0];
            shuju.easy_traffic2 = shuju.easy_traffic[1];
            shuju.biaoqian1     = shuju.biaoqian[0];
            shuju.biaoqian2 = shuju.biaoqian[1];
            shuju.biaoqian3 = shuju.biaoqian[2];
            var shangpuleixingid = that.data.shangpuleixingid;
            ea.each(shangpuleixingid,function(i,v){
              if(v==shuju.types){
                that.setData({ index: i, xianshi:false});
              }
            })
            if(shuju.sex=='男'){
              var items=[
                { name: '1', value: '男' ,checked:true},
                { name: '0', value: '女', checked:false},
              ];
            }else{
              var items = [
                { name: '1', value: '男' ,checked: false},
                { name: '0', value: '女', checked: true },
              ];
            }
            if (shuju.zhuangtai==1){
              that.setData({ index1:0});
            }else{
              that.setData({ index1: 1 });
            }
            that.setData({ shuju: shuju, youtu: true, items: items, xianshi1:false});
          }
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var imgs = wx.getStorageSync('imgs');
    if (imgs) {
      this.setData({
        imgs: imgs,
        youtu: true,
        first_img: imgs[0],  //第一张显示的图片
        keyishangchuang: false,  //隐藏上传图片
      })
      var shuju = this.data.shuju;
      shuju.imgs = imgs;
      console.log(shuju);
      this.setData({ shuju: shuju })
      wx.setStorageSync('imgs', null);
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {


  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },


})