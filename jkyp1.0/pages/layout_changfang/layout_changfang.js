// pages/layout_changfang/layout_changfang.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

    leixing: [   // 类型选择
      {
        id: 0,
        name: "出售"
      },
      {
        id: 1,
        name: "出租"
      },
      {
        id: 2,
        name: "转让"
      },
    ]

  },
  toshangpu: function (e) {
    let id = e.currentTarget.dataset.id
    if (id == 0) {
      wx.navigateTo({
        url: '../layout_changfang_sale/layout_changfang_sale',  //出售
      })

    }
    if (id == 1) {
      wx.navigateTo({
        url: '../layout_changfang_chuzu/layout_changfang_chuzu', //出租
      })


    }
    if (id == 2) {
      wx.navigateTo({
        url: '../layout_changfang_zhuanzu/layout_changfang_zhuanzu', //转租
      })

    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})