// pages/upimg/upimg.js
var ea = require('../../utils/each.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
      imgs:[],
      type:'',
      value:'',
  },

  //点击确定
  chooseImg: function (e) {
    var that = this;
    var imgs = that.data.imgs;
    if(imgs){
      if (imgs.length >= 9) {
        that.setData({
          lenMore: 1
        });
        setTimeout(function () {
          that.setData({
            lenMore: 0
          });
        }, 2500);
        return false;
      }
    }else{
      imgs = [];
    }
    
    
    wx.chooseImage({
      // count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths;
        // console.log(tempFilePaths + '----');
        if ((imgs.length + tempFilePaths.length)>9){
          wx.showToast({
            title: '最多上传9张',
            icon:'none'
          })
        }else{
          ea.each(tempFilePaths,function(i,v){
            imgs.push(v);
          })
        }
        console.log(imgs);
        that.setData({
          imgs: imgs
        });
      }
    });
  },
// 预览图片
previewimg:function(e){
  var index = e.currentTarget.dataset.index;
  var imgs = this.data.imgs;
  console.log(imgs);
  wx.previewImage({
    current: imgs[index],
    urls: imgs,
    success: function (res) {},
    fail: function (res) {},
    complete: function (res) {},
  })
},
// 删除图片
deleteImg:function(e){
  var that = this;
  var imgs = this.data.imgs;
  var index = e.currentTarget.dataset.index;
  wx.showModal({
    title: '提示',
    content: '确定要删除此图片吗？',
    showCancel: true, 
    success: function(res) {
      if(res.confirm){
        console.log('点击确定了');
        imgs.splice(index,1)
      }else if(res.cancel){
        console.log("点击取消了");
        return false;
      }
      that.setData({
        imgs:imgs
      })
    },
    fail: function(res) {},
    complete: function(res) {},
  })
},
 
// 点击确定事件
  sure:function(){
    console.log(this.data.imgs);
    wx.setStorageSync('imgs', this.data.imgs);
    // 如果type为shangpu时跳转至相应的页面
    wx.navigateBack({
      delta:1
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options);
    // var type=options.type;
    // var value = options.value;
    // this.setData({
    //   type:type,
    //   value:value
    // })
    // console.log(this.data.type,this.data.value)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var img = wx.getStorageSync('imgs');
    if(img){
      wx.getStorageSync('imgs',null)
      that.setData({ imgs: img })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})