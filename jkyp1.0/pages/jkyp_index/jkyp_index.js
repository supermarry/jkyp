// pages/jkyp_index/jkyp_index.js
var qqMap = require('../../utils/qqmap-wx-jssdk.min.js');
// var app = getApp();
var qqMapsdk;
const app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wanchen:false,
    latitude:null,
    longitude:null,
    address:null,
    quanju:false,
    loginUrl:app.data.url+'api/api/login',
    getIndexUrl:app.data.url+'api/api/getIndex',
    lunbo:[
      "/assets/banner1.png",
      "/assets/banner1.png"
    ],
    styles:[
      { id: 2, src: "http://jikeyoupu.cybwnt.com/5c457a7a329df1200.png", name: "商铺" },
      { id: 4, src: "http://jikeyoupu.cybwnt.com/5c457a966b8c41661.png", name: "写字楼" },
      { id: 3, src: "http://jikeyoupu.cybwnt.com/5c457a88e3c492300.png", name: "厂房" },
      { id: 1, src: "http://jikeyoupu.cybwnt.com/5c457a6556c6c7859.png", name: "住房" }
    ],
    jktt:[
      
    ],
    youzi:[
     
    ],
    tuijian:[
      
    ]
  },
  gengduode:function(){
    // console.log(this.data.name);
    // console.log(this.data.qu);
    var qu = wx.getStorageSync('qu');
    wx.setStorageSync('qus', qu);
    // wx.setStorageSync('city', this.data.city);
    // wx.setStorageSync('qu', this.data.qu);
    // wx.setStorageSync('fangyuan', '');
    wx.switchTab({
      url: '/pages/jkyp_class/jkyp_class',
    })
  },
  goinfo:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('id', id);
    wx.navigateTo({
      url: '/pages/details/details',
    })
  },
  tiaozhuan:function(e){
    var id = e.target.dataset.id;
    wx.setStorageSync('fenlei_type', id);
    wx.switchTab({
      url: '/pages/jkyp_class/jkyp_class',
    })
  },
  oufenlei:function(){
    wx.switchTab({
      url: '/pages/jkyp_class/jkyp_class',
    })
  },
  quxiangqing:function(e){
    wx.setStorageSync('id', e.currentTarget.dataset.id);
    wx.navigateTo({
      url: '/pages/details/details',
    })
  },
  //跳转搜索
  tosearch:function(){
    wx.navigateTo({
      url: '/pages/search/search',
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    // this.getLocation();
    var city = wx.getStorageSync("city");
    //console.log(city);
    if (city){
      that.setData({ name: city});
    }
    wx.getSetting({
      success: function (res) {
        if (!res.authSetting['scope.userInfo']) {
          wx.navigateTo({
            url: '/pages/index/index',
          })
        } else {
          var openid = wx.getStorageSync('openid');
          if(!openid){
            wx.login({
              success:function(re){
                var code = re.code;
                wx.getUserInfo({
                  success:function(res){
                    var userInfo = res.userInfo;
                    wx.request({
                      url: that.data.loginUrl,
                      method:'post',
                      data: { code: code, nickname: userInfo.nickName, head_portrait: userInfo.avatarUrl},
                      success:function(res){
                        var data = res.data;
                        console.log(data);
                        if (data.code !== 200) {
                          wx.showToast({
                            title: data.msg,
                            icon: 'none'
                          })
                        }
                        wx.setStorageSync('user', data.data);
                        wx.setStorageSync('openid', data.data.openid);
                        that.getindexdata();
                      }
                    })
                  }
                })
              }
            })
          }else{
            that.getindexdata();
          }
        }

      }
    })
    
    
  },
  //跳转求组列表
  qiouzu:function(){
    wx.navigateTo({
      url: '/pages/qiuzulist/qiuzulist',
    })
  },
  getindexdata:function(){
    var openid = wx.getStorageSync('openid');
    var that = this;
    var tiaojian = {};
    tiaojian.city = wx.getStorageSync('city');
    if (that.data.qu){
      tiaojian.qu = that.data.qu;
    }
    tiaojian.openid = openid;
    wx.request({
      url: that.data.getIndexUrl,
      data: tiaojian,
      success:function(res){
        var data =res.data;
        if(data.code==200){
          console.log(data.data);
          //添加轮播图
          var lunbo = data.data.head;
          that.setData({ lunbo: lunbo})
          //获取选项
          var styles = [];
          ea.each(data.data.styles,function(i,v){
            styles.push({ id: v.id, src: v.tubiao, name: v.name})
          })
          that.setData({ styles: styles})
          that.setData({ wanchen: true })
          //添加集客头条
          var jktt = data.data.jktt;
          console.log(jktt);
          that.setData({ jktt: jktt })
          //添加附近
          var fujin = data.data.fujin;
          that.setData({fujin:fujin});
          //添加优质
          var youzi = data.data.youzi;
          that.setData({ youzi: youzi });
          //添加推荐
          var tuijian = data.data.tuijian;
          that.setData({ tuijian: tuijian });
          setTimeout(function(){
            that.setData({quanju:true});
            wx.hideLoading();
          },1000)
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    var city = wx.getStorageSync('city');
    var that = this;
    if(!city){
      ea.getLocation(function(city){
        console.log(city);
        that.setData({name:city.shi});
        that.setData({ qu: city.xian});
        wx.setStorageSync('city', city.shi);
        wx.setStorageSync('qu', city.xian);
        that.onShow();
      })
    }
  },  // 获取当前城市的名称
  getLocation:function(){
    var that = this;
    wx.getLocation({
      type:'wgs84',  
      success: function(res) {
          // console.log(res)
          var speed = res.speed;
          var accuracy = res.accuracy;
          var latitude = res.latitude;
          var longitude = res.longitude;
          that.setData({
            latitude:latitude,
            longitude:longitude
          })
          // 地址解析
          var app = getApp()
          var test = new qqMap({
            key: app.data.qqmapkey
          });
          test.reverseGeocoder({
            location:{
              latitude: latitude,
              longitude: longitude
          },
            success: function (res){
              // console.log(res);
              if(res.status==0){
                var address = res.result.address_component.city;
                // console.log(address)
                  that.setData({
                    name:address
                  });
                wx.setStorageSync('city', address);
              }
          }
        })
      },
    })
  },
  // 跳转至优质房源信息
  gotoyouzhi:function(){
    wx.setStorageSync('youzhi', 1);
    wx.switchTab({
      url: '/pages/jkyp_class/jkyp_class',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  //跳转选择城市
  tochoose: function () {
    wx.navigateTo({
      url: '../choose_city/choose_city',
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})