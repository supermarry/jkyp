// pages/my_favorites/my_favorites.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
getshouchanginfo:app.data.url+'api/api/shouchanginfo',
shuju:[],
quanju:false,
    shouchangout: app.data.url +'api/api/shouchang',
    shouchangrent: app.data.url +'api/api/shouchangRent'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
wx.showLoading({
  title: '加载中',
})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var openid = wx.getStorageSync('openid');
    var that = this;
    wx.request({
      url: that.data.getshouchanginfo,
      data:{openid:openid},
      success:function(res){
        var data = res.data;
        if(data.code==200){
          var shuju = [];
          ea.each(data.data,function(i,v){
            v.shou = true;
            shuju.push(v);
          })
          console.log(shuju);
          that.setData({ shuju: shuju})
          that.setData({quanju:true});
          wx.hideLoading();
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  quout:function(e){
var id = e.currentTarget.dataset.id;
    wx.setStorageSync('id', id);
    wx.navigateTo({
      url: '/pages/details/details',
    })
  },
  qurent:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('rent_id', id);
    // console.log(id)
    wx.navigateTo({
      url: '/pages/qiuzu_detail/qiuzu_detail',
    })
  },
  shouchangout:function(e){
    var id = e.currentTarget.dataset.id;
    var that = this;
    var openid= wx.getStorageSync('openid');
    wx.request({
      url: that.data.shouchangout,
      data:{openid:openid,id:id},
      success:function(res){
        var data = res.data;
        console.log(data)
        if(data.code==200){
          var shuju = that.data.shuju;
          var xin = [];
          ea.each(shuju,function(i,v){
            if(v.id==id&&v.biao=='out'){
              v.shou = data.data;
            }
            xin.push(v);
          })
          that.setData({shuju:xin});
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },
  shouchangrent:function(e){
    var id = e.currentTarget.dataset.id;
    var that = this;
    var openid = wx.getStorageSync('openid');
    wx.request({
      url: that.data.shouchangrent,
      data: { openid: openid, id: id },
      success: function (res) {
        var data = res.data;
        console.log(data);
        if (data.code == 200) {
          var shuju = that.data.shuju;
          var xin = [];
          ea.each(shuju, function (i, v) {
            if (v.id == id && v.biao == 'rent') {
              v.shou = data.data;
            }
            xin.push(v);
          })
          that.setData({ shuju: xin });
        } else {
          wx.showToast({
            title: data.msg,
            icon: 'none'
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})