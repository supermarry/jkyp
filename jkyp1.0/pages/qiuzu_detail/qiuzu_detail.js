// pages/qiuzu_detail/qiuzu_detail.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getrentinfoUrl: app.data.url +'api/api/getrentinfo',
    shouchangrent: app.data.url +'api/api/shouchangRent',
    guwen_phone:'',
    sfsc:false,
    info:{
      type:'',
      quyu:'',
      qu:'',
      hope_area:'',
      min_mianji:'',
      max_mianji:'',
      min_jiage:'',
      max_jiage:'',
      dir:''
    },
    xiangshi:[],
  },
  tocollect:function(e){
console.log(e)
var that = this;
var id = e.currentTarget.dataset.id;
var openid = wx.getStorageSync('openid');
// console.log(id)
wx.request({
  url: that.data.shouchangrent,
  data:{id:id,openid:openid},
  success:function(res){
    var data = res.data;
    if(data.code==200){
      if(data.data==1){
        wx.showToast({
          title: '收藏成功',
        })
      }else{
        wx.showToast({
          title: '取消收藏成功',
          icon:'none'
        })
      }
      that.setData({ sfsc:data.data})
    }else{
      wx.showToast({
        title: data.msg,
        icon:'none'
      })
    }
  }
})
  },
  tiaoxiangqing:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('rent_id', id)
    wx.navigateTo({
      url: '/pages/qiuzu_detail/qiuzu_detail',
    })
    console.log(id)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  toguwen:function(){
    var phone = this.data.guwen_phone;
    wx.makePhoneCall({
      phoneNumber: phone,
    })
  },
  tofangdong:function(){
    var phone = this.data.info.phone;
    wx.makePhoneCall({
      phoneNumber: phone,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var rent_id = wx.getStorageSync('rent_id');
    var openid = wx.getStorageSync('openid');
    var lioulan = wx.getStorageSync('lioulan');
    var shijian = ea.timeToarray();
    if(lioulan.length>0){
      var array = [];
      ea.each(lioulan,function(i,v){
        if(!(v.id==rent_id&&v.type=='rent')){
          array.push(v);
        }
      })
      array.push({id:rent_id,type:'rent',year:shijian.year,month:shijian.month,day:shijian.day});
      lioulan = array;
    }else{
      lioulan = { id: rent_id, type: 'rent', year: shijian.year, month: shijian.month, day: shijian.day};
    }
    wx.setStorageSync('lioulan', lioulan)
    wx.request({
      url: that.data.getrentinfoUrl,
      data:{openid:openid,rent_id:rent_id},
      success:function(res){
        var data = res.data;
        var shuju = data.data.data;
        that.setData({info:shuju})
        that.setData({ xiangshi: data.data.xiangshi})
        that.setData({ guwen_phone: data.data.guwen.guwen_phone})
        that.setData({ sfsc: data.data.shoucang})
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})