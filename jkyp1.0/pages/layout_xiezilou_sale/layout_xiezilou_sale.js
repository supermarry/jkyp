// pages/layout_xiezilou_sale/layout_xiezilou_sale.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addoutUrl: app.data.url +'api/newapi/addout',
    changeouturl: app.data.url + 'api/newapi/changeout',
    shuju:{sex:'女'},
    getxzltypeurl:app.data.url+'api/newapi/gettype',
    // 男女状态
    items: [
      { name: 1, value: '男' },
      { name: 0, value: '女', checked: 'true' },
    ],
    easy_traffic1:'',
    easy_traffic2:'',
    xzl_pare: app.data.url + 'api/classify/xzl_sale',
    backimg: "http://jikeyoupu.cybwnt.com/backgro.jpg", //背景图图片url地址
    keyishangchuang:true,
    region:['全部','全部','全部'],
    xianshi:true,
    xianshi1:true,
    xzlleixing: ["纯写字楼", "商业综合体", "商务公寓", "商务酒店"],  //写字楼类型
    xzlleixingid: [],  //写字楼类型id
    index: "",    //写字楼类型点击选择的第几项，通过index可以获取id
    xianshi: true,   //请选择控制 默认为true
    peitao: [  //配套设施
      {
        id: 0,
        name: "客梯",
        checked: 'false'
      },
      {
        id: 1,
        name: "货梯",
        checked: 'false'
      },

    ],
    kongzhi:1,
    
  },
  tomiaoshu: function () {
    wx.navigateTo({
      url: '../describe/describe',
    })
  },
  upload:function(){
    var imgs = this.data.shuju.imgs;
    if(imgs){
      wx.setStorageSync('imgs', imgs);
    }
    wx.navigateTo({
      url: '/pages/upimg/upimg',
    })
  },
  //修改
  xiougai:function(){
    if (this.data.kongzhi == 0) {
      return;
    }
    this.setData({ kongzhi: 0 })
    var that = this;
    var shuju = that.data.shuju;
    var biaoqian = [];
    if (shuju.biaoqian1) {
      biaoqian.push(shuju.biaoqian1);
    }
    if (shuju.biaoqian2) {
      biaoqian.push(shuju.biaoqian2);
    }
    if (shuju.biaoqian3) {
      biaoqian.push(shuju.biaoqian3);
    }
    shuju.biaoqian = biaoqian;
    var easy_traffic = [];
    if (shuju.easy_traffic1) {
      easy_traffic.push(shuju.easy_traffic1);
    }
    if (shuju.easy_traffic2) {
      easy_traffic.push(shuju.easy_traffic2);
    }
    shuju.easy_traffic = easy_traffic;
    var peitaosesi = [];
    var peitao = that.data.peitao;
    ea.each(peitao, function (i, v) {
      if (v.checked == true) {
        peitaosesi.push(v.id);
      }
    })
    shuju.peitao = peitaosesi;
    var shu = ea.yanzhen(shuju, {
      imgs: "请选择图片",
      sheng: '请输入详细区域',
      shi: '请输入详细区域',
      qu: '请输入详细区域',
      regin: '请输入地址',
      proportion: '请输入面积',
      price: '请输入售价',
      types: '请输入类型',
      title: '请输入标题',
      dir: '请输入描述',
      last_name: '请输入联系人姓',
      lxr_phone: '请输入联系人手机'
    });
    if (shu) {
      wx.showToast({
        title: shu,
        icon: 'none'
      });
      that.setData({ kongzi: 1 })
      return;
    }
    var img = shuju.img;
    var imgs = shuju.imgs;
    var delimg = [];
    var newimg = [];
    ea.each(imgs, function (i, v) {
      if (img.indexOf(v) == -1) {
        newimg.push(v);
      }
    })
    ea.each(img, function (i, v) {
      if (imgs.indexOf(v) == -1) {
        delimg.push(v);
      }
    })
    shuju.delimg = delimg;
    shuju.newimg = newimg;
    shuju.openid = wx.getStorageSync('openid');
    ea.upfile(newimg, function (v) {
      shuju.newimg = v;
      wx.request({
        url: that.data.changeouturl,
        data: shuju,
        method: 'post',
        success: function (res) {
          var data = res.data;
          if (data.code == 200) {
            wx.showToast({
              title: data.msg,
              icon: 'success'
            })
            setTimeout(function () {
              wx.navigateBack({
                delta: 1
              })
            }, 1000)
          } else {
            that.setData({kongzhi:1})
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        }
      })
    })
  },
  //发布
  fabu:function(){
    if (this.data.kongzhi == 0) {
      return;
    }
    this.setData({ kongzhi: 0 })
    var that = this;
    var shuju = that.data.shuju;
    var biaoqian = [];
    if(shuju.biaoqian1){
      biaoqian.push(shuju.biaoqian1);
    }
    if (shuju.biaoqian2) {
      biaoqian.push(shuju.biaoqian2);
    }
    if (shuju.biaoqian3) {
      biaoqian.push(shuju.biaoqian3);
    }
    shuju.biaoqian = biaoqian;
    var easy_traffic = [];
    if (shuju.easy_traffic1){
      easy_traffic.push(shuju.easy_traffic1);
    }
    if (shuju.easy_traffic2) {
      easy_traffic.push(shuju.easy_traffic2);
    }
    shuju.easy_traffic = easy_traffic;
    var peitaosesi = [];
    var peitao = that.data.peitao;
    ea.each(peitao,function(i,v){
      if(v.checked==true){
        peitaosesi.push(v.id);
      }
    })
    shuju.peitao = peitaosesi;
    shuju.type = 4;
    shuju.out_type = 2;
    var shu = ea.yanzhen(shuju,{
      imgs:"请选择图片",
      sheng:'请输入详细区域',
      shi: '请输入详细区域',
      qu: '请输入详细区域',
      regin:'请输入地址',
      proportion:'请输入面积',
      price:'请输入售价',
      types:'请输入类型',
      title:'请输入标题',
      dir:'请输入描述',
      last_name:'请输入联系人姓',
      lxr_phone:'请输入联系人手机'
    });
    if(shu){
      wx.showToast({
        title: shu,
        icon:'none'
      });
      that.setData({ kongzi: 1 })
      return;
    }
    ea.upfile(shuju.imgs,function(v){
      shuju.imgs = v;
      shuju.openid = wx.getStorageSync('openid');
      wx.request({
        url: that.data.addoutUrl,
        method:'post',
        data:shuju,
        success:function(res){
          var data = res.data;
          if(data.code==200){
            wx.showToast({
              title: '发布成功'
            })
            setTimeout(function(){
              wx.switchTab({
                url: '/pages/layout/layout',
              })
            },1000)
          }else{
            that.setData({kongzhi:1})
            wx.showToast({
              title: data.msg,
              icon:'none'
            })
          }
        }
      })
    })
  },

  // 用户选择的配套设施,在点击提交的
  mypeitao: function (e) {
    var id = e.currentTarget.dataset.id;
    var peitao = this.data.peitao;
    var newpeitao = [];
    ea.each(peitao,function(i,v){
      if(v.id==id){
        if(v.checked==true){
          v.checked = false;
        }else{
          v.checked = true;
        }
      }
      newpeitao.push(v)
    })
    this.setData({ peitao:newpeitao});
  },


  addInput:function(e){
    var str = e.detail.value;
    let item = e.currentTarget.dataset.model;
    var shuju = this.data.shuju;
    shuju[item] = str;
    console.log(shuju);
    this.setData({shuju:shuju});
  },

 
  bindPickerChange:function(e){
    var array = e.detail.value;
    var shuju = this.data.shuju;
    shuju.sheng = array[0];
    shuju.shi = array[1];
    shuju.qu = array[2];
    console.log(shuju);
    this.setData({ shuju: shuju, region:array});
  },
  //获取类型
  bindtype:function(e){
    var index = e.detail.value;
    var xzlleixingid = this.data.xzlleixingid;
    var shuju = this.data.shuju;
    shuju.types = xzlleixingid[index];
    this.setData({ index: index, xianshi1:false});
    console.log(shuju);
    this.setData({shuju:shuju});
  },
  radioChange:function(e){
    var id = e.detail.value;
    var shuju = this.data.shuju;
    if(id==1){
      shuju.sex='男';
    }else{
      shuju.sex = '女';
    }
    console.log(shuju);
    this.setData({shuju:shuju})
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setStorageSync('imgs', null);
    wx.setStorageSync('miaoshu', null)
    var that = this;
    var tiaojian = {};
    tiaojian.openid = wx.getStorageSync('openid');
    tiaojian.pid = 4;
    if(options.id){
      tiaojian.id = options.id;
    }
    wx.request({
      url: that.data.getxzltypeurl,
      data: tiaojian,
      method:'post',
      success:function(res){
        var data = res.data;
        if(data.code==200){
          console.log(data.data)
          var xzlleixing = [];
          var xzlleixingid = [];
          // console.log(data.data.data);
          ea.each(data.data.type,function(i,v){
            xzlleixing.push(v.name);
            xzlleixingid.push(v.id)
          })
          that.setData({ xzlleixing: xzlleixing, xzlleixingid: xzlleixingid});
          var peitao = [];
          ea.each(data.data.peitao,function(i,v){
            peitao.push({id:v.id,name:v.title,checked:false});
            that.setData({ peitao: peitao})
          })
          if(data.data.data){
            var shuju = data.data.data;
            shuju.imgs = shuju.img;
            shuju.easy_traffic1 = shuju.easy_traffic[0];
            shuju.easy_traffic2 = shuju.easy_traffic[1];
            shuju.biaoqian1 = shuju.biaoqian[0];
            shuju.biaoqian2 = shuju.biaoqian[1];
            shuju.biaoqian3 = shuju.biaoqian[2];
            var region = [];
            region.push(shuju.sheng)
            region.push(shuju.shi)
            region.push(shuju.qu);
            wx.setStorageSync('miaoshu', shuju.dir);
            var xzlleixingid = that.data.xzlleixingid;
            ea.each(xzlleixingid,function(i,v){
              if(v==shuju.types){
                that.setData({ index: i, xianshi1:false})
              }
            })
            var peitao = that.data.peitao;
            var newpeitao = [];
            ea.each(peitao, function (i, v) {
              if (shuju.peitao.indexOf(v.id) == -1) {
                v.checked = false;
              } else {
                v.checked = true;
              }
              newpeitao.push(v);
            })
            if (shuju.sex == '男') {
              var items = [
                { name: '1', value: '男', checked: true },
                { name: '0', value: '女', checked: false },
              ];
            } else {
              var items = [
                { name: '1', value: '男', checked: false },
                { name: '0', value: '女', checked: true },
              ];
            }
            that.setData({ shuju: shuju, youtu: true, region: region,peitao:newpeitao,items:items});
          }
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var imgs = wx.getStorageSync('imgs');
    var shuju = this.data.shuju;
    if (imgs) {
      this.setData({
        imgs: imgs,
        youtu: true,
        first_img: imgs[0],  //第一张显示的图片
        keyishangchuang: false,  //隐藏上传图片
      })
      shuju.imgs = imgs;
      // this.setData({ shuju: shuju })
      wx.setStorageSync('imgs', null);
    }
    var str = wx.getStorageSync('miaoshu');
    shuju.dir = str;
    console.log(shuju);
    this.setData({shuju:shuju});
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})