// pages/my/my.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nickname:'嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻',
    touxiang:'/assets/ava.png',
    shouchang:0,
    fabu:0,
    renzhen:'未认证',
    kefu_phone: '028-87643248',
    getUserInfoUrl:app.data.url+'api/api/getuserinfo'
  },
  // 跳转至我的收藏
  tofav:function(){
    wx.navigateTo({
      url: '../my_favorites/my_favorites',
    })
  },
  // 跳转至我的发布
  tolayout:function(){
    wx.navigateTo({
      url: '../my_layout/my_layout',
    })
  },
  // 跳转至我的消息
  tomessage:function(){
    wx.navigateTo({
      url: '../my_message/my_message',
    })
  },
  //跳转至个人认证
  torenzheng:function(){
    wx.navigateTo({
      url: '../my_certification/my_certification',
    })
  },
  //跳转至最近浏览
  torecent:function(){
    wx.navigateTo({
      url: '../my_recent/my_recent',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    wx.getUserInfo({
      success:function(res){
        var userinfo = res.userInfo;
        that.setData({nickname:userinfo.nickName,touxiang:userinfo.avatarUrl})
      }
    })
    var openid = wx.getStorageSync('openid');
    wx.request({
      url: that.data.getUserInfoUrl,
      data:{openid:openid},
      success:function(res){
        var data = res.data;
        console.log(data);
        if(data.code==200){
          that.setData({fabu:data.data.fabu});
          console.log(data.data.kefu)
          if(data.data.kefu){
            that.setData({ kefu_phone: data.data.kefu})
          }
          if (data.data.shouchang){
            that.setData({ shouchang: data.data.shouchang })
          }
          var user = data.data.user;
          wx.setStorageSync('user', user)
          var num = 0;
          var str = '';
          if(user.user_name){
            num++;
          }
          if(user.sex){
            num++;
          }
          if (user.phone){
            num++;
          }
          if(user.hangye){
            num++;
          }
          if (user.company){
            num++;
          }
          if (user.position){
            num++;
          }
          if (user.love){
            num++;
          }
          if(num==0){
            str = '未认证';
          }else{
            str = parseInt(num/7*100)+'%';
          }
          that.setData({ renzhen:str})
        
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})