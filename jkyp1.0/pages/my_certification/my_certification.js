// pages/certification/my_certification.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    changeuser:app.data.url+'api/api/changeuser',
user:{
  user_name:'',
  sex:'',
  phone:'',
  hangye:'',
  company:'',
  position:'',
  love:''
},
    user_name:'',
    sex: '',
    phone: '',
    hangye: '',
    company: '',
    position: '',
    love: ''
  },
  tijiao:function(){
    var that = this;
    var user_name = that.data.user_name;
    var sex = that.data.sex;
    var phone = that.data.phone;
    var hangye = that.data.hangye;
    var company = that.data.company;
    var position = that.data.position;
    var love = that.data.love;
    var openid = wx.getStorageSync('openid');
    wx.request({
      url: that.data.changeuser,
      data: { openid: openid, love: love, position: position, company: company, hangye: hangye, phone: phone, sex: sex, user_name: user_name},
      success:function(res){
        var data = res.data;
        if(data.code==200){
          console.log(data)
          wx.showToast({
            title: '修改成功'
          })
          wx.setStorageSync('user', data.data);
          setTimeout(function(){
            wx.switchTab({
              url: '/pages/my/my',
            })
          },1000)
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },
  /**
   * 添加真实姓名
   */
  adduser_name:function(e){
console.log(e.detail.value);
    this.setData({ user_name:e.detail.value})
  },
  /**
  * 添加真实姓名
  */
  addsex: function (e) {
    console.log(e.detail.value);
    this.setData({ sex: e.detail.value })
  },
  /**
  * 添加真实姓名
  */
  addphone: function (e) {
    console.log(e.detail.value);
    this.setData({ phone: e.detail.value })
  },
  /**
  * 添加真实姓名
  */
  addhangye: function (e) {
    console.log(e.detail.value);
    this.setData({ hangye: e.detail.value })
  },
  /**
  * 添加真实姓名
  */
  addcompany: function (e) {
    console.log(e.detail.value);
    this.setData({ company: e.detail.value })
  },
  /**
  * 添加真实姓名
  */
  addposition: function (e) {
    console.log(e.detail.value);
    this.setData({ position: e.detail.value })
  },
  /**
  * 添加真实姓名
  */
  addlove: function (e) {
    console.log(e.detail.value);
    this.setData({ love: e.detail.value })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var user = wx.getStorageSync('user');
    var that = this;
    that.setData({user:user});
    that.setData({
      user_name: user.user_name,
      sex: user.sex,
      phone: user.phone,
      hangye: user.hangye,
      company: user.company,
      position: user.position,
      love: user.love
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})