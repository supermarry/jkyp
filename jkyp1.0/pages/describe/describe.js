// pages/describe/describe.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
value:'',
  },
  tianjiainput:function(e){
    var str = e.detail.value;
    this.setData({value:str});
    console.log(str);
  },
  qingchu:function(){
    console.log('清除了')
    this.setData({value:''});
    wx.setStorageSync('miaoshu', '');
  },
  
  wanchen:function(){
    var str = this.data.value;
    wx.setStorageSync('miaoshu', str);
    console.log(str)
    wx.navigateBack({
      delta: 1
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var str = wx.getStorageSync('miaoshu');
    // console.log(str);
    if(str){
      this.setData({value:str});
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})