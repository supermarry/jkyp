// pages/details/details.js
var ea = require('../../utils/each.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    haibao:'',
    hideModal: true, //模态框的状态  true-隐藏  false-显示
    animationData: {},//
    hideModals:true,
    shuju: {},
    shouchangUrl:app.data.url+'api/api/shouchang',
    geterweimaurl: app.data.url +'api/newapi/geterweima',
    quanju:true,
    sfsc:true,
    canvas:false,
    srcs:'../../assets/backgro.jpg',
    guwen:'',
    fangdong:'',
    peitao:[
      
    ],
    erweima:'',
    xiangshinum:0,
    getoutinfoUrl: app.data.url +'api/api/outinfo',
    type:1,//1是住宅2是其他
    out_type:2,//1是出租,2是出售，3是转让,
    xiangqings:'',
    xiangqing:{
        id:1,
        banners: ['/assets/x1.jpg', '/assets/banner1.png',], //详情轮播图数组
        name:"地铁口美容院带客源优转xsy", //房源名称
        weizhi:"锦江区蜀泰商厦", //具体位置
        yuezujin:68000, //月租金
        zhuanrangfei:"面议",// 转让费
        mianji:200,               //面积
        miankuan:6.5,             // 面宽
        cengao:6,          // 层高
        leixing:"临街门面",// 类型
        shoujia:0,
        fukuanfangshi:"押一付三",// 付款方式
        lishijingying:"美容院",// 历史经营
        jingshen:6.5, //进深
        louceng:"12/32", //楼层
        status:"经营中", //经营状态
        shengyuzuqi:60, // 剩余租期
      dir: "",
      xiangsituijian: [   //相似推荐数组
        
      ]

      },
    xiangsituijian:[],
    latitude: 23.099994,
    longitude: 113.324520,
    markers: [{
      id: 1,
      latitude: 23.099994,
      longitude: 113.324520,
      name: 'T.I.T 创意园'
    }],
    covers: [{
      latitude: 23.099994,
      longitude: 113.344520,
    }, {
      latitude: 23.099994,
      longitude: 113.304520,
    }],
    // 配套设施
    dianti:false,
    huoti:true,
    kongtiao:false,
    tingchewei:false,
    tianranqi:true,
    wangluo:true,
    nuanqi:true,
    futi:true,
    shangshui:false,
    xiashui:false,
    paiyan:true,
    paiwu:false,
    guanmei:true,
    sanba:false,
    keminghuo:false,
    waibaiqu:false,
    bodys:true,
  },
//相似推荐点击图片跳转至详情页面
todetail:function(e){
  var id = e.target.dataset.id;
  wx.setStorageSync('id', id)
  wx.navigateTo({
    url: '/pages/details/details',
 })
},
// 点击分享按钮事件
toshare:function(){
  this.onShareAppMessage();
},

  // 显示遮罩层
  showModal: function () {
    var that = this;
    that.setData({
      hideModal: false
    })
    var animation = wx.createAnimation({
      duration: 600,//动画的持续时间 默认400ms   数值越大，动画越慢   数值越小，动画越快
      timingFunction: 'ease',//动画的效果 默认值是linear
    })
    this.animation = animation
    setTimeout(function () {
      that.fadeIn();//调用显示动画
    }, 200)
  },

  // 隐藏遮罩层
  hideModal: function () {
    var that = this;
    var animation = wx.createAnimation({
      duration: 800,//动画的持续时间 默认400ms   数值越大，动画越慢   数值越小，动画越快
      timingFunction: 'ease',//动画的效果 默认值是linear
    })
    this.animation = animation
    that.fadeDown();//调用隐藏动画   
    setTimeout(function () {
      that.setData({
        hideModal: true
      })
    }, 720)//先执行下滑动画，再隐藏模块

  },
  // 隐藏遮罩层
  hideModals: function () {
    var that = this;
    var animation = wx.createAnimation({
      duration: 800,//动画的持续时间 默认400ms   数值越大，动画越慢   数值越小，动画越快
      timingFunction: 'ease',//动画的效果 默认值是linear
    })
    this.animation = animation
    that.fadeDown();//调用隐藏动画   
    setTimeout(function () {
      that.setData({
        hideModal: true
      })
    }, 720)//先执行下滑动画，再隐藏模块

  },


  //动画集
  fadeIn: function () {
    this.animation.translateY(0).step()
    this.setData({
      animationData: this.animation.export()//动画实例的export方法导出动画数据传递给组件的animation属性
    })
  },

  fadeDown: function () {
    this.animation.translateY(300).step()
    this.setData({
      animationData: this.animation.export(),
    })
  }, 







// 点击搜藏事件
tocollect:function(e){
  var that = this;
  var id = e.currentTarget.dataset.id;
  var openid = wx.getStorageSync('openid');
wx.request({
  url:that.data.shouchangUrl,
  data:{openid:openid,id:id},
  success:function(res){
    var data = res.data;
    console.log(data);
    if(data.code==200){
      if(data.data==1){
        wx.showToast({
          title: '收藏成功'
        })
      }else{
        wx.showToast({
          title: '取消收藏成功',
          icon:'none'
        })
      }
      that.setData({ sfsc: data.data});
    }else{
      wx.showToast({
        title: data.msg,
        icon:'none'
      })
    }
  }
})
},
// 点击业务顾问事件
toguwen:function(){
  var that = this;
  console.log(that.data.guwen);
  wx.makePhoneCall({
    phoneNumber: that.data.guwen,
  })
},
// 点击房东事件
tofangdong: function () {
  var that = this;
  console.log(that.data.fangdong)
  wx.makePhoneCall({
    phoneNumber: that.data.fangdong,
  })
},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.id);
    if(options.id){
      console.log(options.id);
      wx.setStorageSync('id', options.id);
    }
    wx.showLoading({
      title: '加载中',
    })
  },
  xuanchuan:function(){
    var that = this;
    wx.request({
      url: that.data.geterweimaurl,
      method:'post',
      data: { id: that.data.shuju.id},
      success: function (res) {
        console.log(res);
        // that.setData({ erweima: true, srcs: res.data })
      }
    })
  },
  yingchang:function(){
    var that = this;
    this.setData({ erweima:false});
    var src = that.data.srcs;
    console.log(src);
    wx.downloadFile({
      url: src,
      success:function(res){
        console.log(res.tempFilePath);
        var path = res.tempFilePath;
        wx.saveImageToPhotosAlbum({
          filePath: path,
          success:function(res){
            wx.showToast({
              title: '保存成功',
            })
          },fail:function(res){
            wx.showToast({
              title: '保存失败',
            })
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function (e) {
    this.ctx = wx.createMapContext("mymap")
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var id = wx.getStorageSync('id');
    var lioulan = wx.getStorageSync('lioulan');
    console.log(id);
    console.log(lioulan);
    var shijian = ea.timeToarray();
    if(lioulan.length>0){
      var array = [];
      ea.each(lioulan,function(i,v){
        // console.log(lioulan)
        if(!(v.id==id&&v.type=='out')){
          array.push(v);
        }
      })
      array.push({ id: id,type:'out', year: shijian.year, month: shijian.month, day: shijian.day });
      lioulan = array;
    }else{
      lioulan = [{id:id,type:'out',year:shijian.year,month:shijian.month,day:shijian.day}];
    }
    wx.setStorageSync('lioulan', lioulan)
    var zuijinout = wx.getStorageSync('zuijinout');
    // console.log(zuijinout)
    if (zuijinout.length>0){
      if (zuijinout.indexOf(id)==-1){
        zuijinout.push(id);
      }
      wx.setStorageSync('zuijinout', zuijinout)
    }else{
      zuijinout = [];
      zuijinout.push(id);
      wx.setStorageSync('zuijinout', zuijinout)
    }
    var openid = wx.getStorageSync('openid');
    wx.request({
      url: that.data.getoutinfoUrl,
      data:{openid:openid,id:id},
      success:function(res){
        var data = res.data;
        // console.log(data);
        if(data.code==200){
          var shuju = data.data.data;
          var zhenzhe = '/[\\r\\n]/';
          // shuju.dir = shuju.dir.split(zhenzhe);
          console.log(shuju);
          that.setData({shuju:shuju});
          // console.log(shuju);
          that.setData({ sfsc:data.data.shoucang})
          that.setData({ fangdong: shuju.lxr_phone})
          that.setData({ guwen: data.data.guwen[0].guwen_phone})
          //设置地图
          if(shuju.house_name){
            var address = shuju.sheng + shuju.shi + shuju.qu + shuju.house_name;
          }else{
            var address = shuju.sheng + shuju.shi + shuju.qu + shuju.regin;
          }
          console.log(address);
          ea.strToJingWei(address,function(data){
            // console.log(data)
            that.setData({ latitude: data.latitude, longitude: data.longitude});
            that.setData({ markers: [{ latitude: data.latitude, longitude: data.longitude,name:shuju.house_name}]});
          })
          var xiangshi = data.data.xiangshi;
          that.setData({ xiangshinum:xiangshi.length})
          var xiangsituijian = [];
          xiangsituijian = xiangshi;
          that.setData({ xiangsituijian: xiangsituijian});
          var peitao = [];
          var peitaos = shuju.peitao;
          var tr = '';
          ea.each(data.data.peitao,function(i,v){
            if(peitaos.indexOf(v.id)!==-1){
              tr = true;
            }else{
              tr=false;
            }
            peitao.push({ id: v.id, name: v.title, img: v.you_tubiao, imgs: v.wu_tubiao, shifou:tr})
          });
          // console.log(peitao)
          that.setData({ peitao: peitao});
          that.setData({quanju:true});
          wx.hideLoading();
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  xuanzaifenxiang:function(){
    console.log('选择分享');
    this.showModal();

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  onReady:function(){
    const context = wx.createCanvasContext('firstCanvas')
    context.drawImage('https://jike.cybwnt.com/uploads/11.jpg', 15, 20,200, 150); //1
    context.drawImage('/assets/lookfor.png',-20,170, 170, 80); //2
    context.drawImage('/assets/15517541643346.jpg ', 145, 180, 70, 70); //3
    context.drawImage('/assets/save.png', 60, 280, 100, 30); //4
    context.draw()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  haibao:function(){
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      url: that.data.geterweimaurl,
      method: 'post',
      data: { id: that.data.shuju.id ,imgs:that.data.shuju.imgs[0]},
      success: function (res) {
        var erweima = res.data;
        console.log(erweima);
        var imgs = that.data.shuju.imgs[0];
        that.setData({ haibao: erweima})
        that.setData({ canvas: true ,bodys:false});
        wx.hideLoading();
        // that.setData({ erweima: true, srcs: res.data })
      }
    })
  },
  baocuntupian:function(){
    var path = this.data.haibao;
    var that = this;
    wx.showLoading({
      title: '保存中',
    })
    wx.downloadFile({
      url:path,
      success:function(res){
        var tempFilePath = res.tempFilePath;
        wx.saveImageToPhotosAlbum({
          filePath: tempFilePath,
          success:function(re){
            wx.hideLoading();
            wx.showToast({
              title: '保存成功',
              icon:'success'
            });
            that.setData({ canvas:false});
            that.hideModal();
          }
        })
      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var str = this.data.shuju.title;
    // this.setData({erweima:true});
    console.log(str);
    this.hideModal();
    return {
      title: str
    }
  }
})