// pages/jkyp_class/jkyp_class.js
const app = getApp();
var ea = require('../../utils/each.js');
Page({
  
  /**
   * 页面的初始数据
   */
  data: {  
    dangqian: 0,// 初始页面显示全部
    // getclassUrl: app.data.url + '/api/Classify/getclass',
    getclassUrls: app.data.url + '/api/newapi/getclass',
    quanju:false,
    shuju:[],
    tiaojian:{},
    page: 1,
    fenlei:[],
    fenleiid:[],
    shi: '',
    type: '',
    qu: '',
    min_mianji: 0,
    max_mianji: '',
    fangyuanid: 0,
    index:'',
    xianshi:true,

    index1: '',
    xianshi1: true,

    index2: '',
    xianshi2: true,

    index3: '', 
    xianshi3: true,

    navs: [ //分类数组
      {id:50,value:'quanbu',name:"全部",checked:true},
      { id: 2, value: 'shangpu', name: "商铺", checked: false},
      { id: 4, value: 'xizilou', name: "写字楼", checked: false},
      { id: 3, value: 'cangku', name: "厂房土地仓库", checked: false},
      { id: 1, value: 'ershoufa', name: "二手房", checked: false},
      // {value: 'qiuzu',name: "求租"},
    ],
  //条件选择数组[地区，分类，面积，更多]
  // 1.1地区数组
    array:[
      '成都', '锦江区', '武侯区','天府新区'
    ],
    arrayid:[11,12,13,14],
    // 1.2分类数组
    array1:[
      '不限',"出租","出售",'转让'
    ],
    array1id:[],
    // 1.3面积数组
    array2:[
      "不限", "小于20m²", "20m²-50m²", "50m²-100m²", "100m²-500m²", "500m²以上"
    ],
    array2id: [],
    // 1.4更多数组
    array3:[
      "不限", "商业街铺","档口摊位社区底商","社区底商","临街门店"
    ],
    array3id: [],
    // 搜索结果数组
    fenlei: [

    ]
    
  },
  gosearch:function(){
wx.navigateTo({
  url: '/pages/search/search',
})
  },
  // 获得当前的显示导航，并加上激活类
  getvalue:function(e){
    var newrt = e.target.dataset.idx;
    var id = e.target.dataset.id;
    console.log(id)
    var navs = this.data.navs;
    var newnavs = [];
    ea.each(navs,function(i,v){
      if(v.id==id){
        v.checked = true;
      }else{
        v.checked = false;
      }
      newnavs.push(v);
    })
    this.setData({navs:newnavs});
    // this.setData(shuju);
    if(id==50){
      var tiaojian = this.data.tiaojian;
      tiaojian.type = '';
    }else{
      var tiaojian = this.data.tiaojian;
      tiaojian.type = id;
    }
    
    this.setData({ tiaojian: tiaojian});
    this.setData({shuju:[]});
    this.setData({page:1});
    this.getclass(1)

  },
 
  //分类选择
  bindPickerChange(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    var index = e.detail.value;
    var array = this.data.array;
    this.setData({
      index: e.detail.value,
      xianshi: false
    });
    var tiaojian = this.data.tiaojian;
    if(index==0){
      tiaojian.qu = '';
    }else{
      tiaojian.qu = array[index];
    }
    this.setData({ tiaojian:tiaojian,page:1,shuju:[]});
    this.getclass(1);
  },
  bindPickerChange1(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index1: e.detail.value,
      xianshi1: false
    })
    var index = e.detail.value;
    var out_type = '';
    if(index==0){
      out_type = '';
    }else if(index==1){
      out_type = 1;
    }else if(index==2){
      out_type = 2;
    }else{
      out_type = 3;
    }
    var tiaojian = this.data.tiaojian;
    tiaojian.out_type = out_type;
    this.setData({tiaojian:tiaojian,page:1,shuju:[]});
    this.getclass(1);
  },
  bindPickerChange2(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index2: e.detail.value,
      xianshi2: false

    })
    var index = e.detail.value;
    var min_mianji = '';
    var max_mianji = '';
    if(index==1){
      max_mianji = 20;
    }else if(index==2){
      min_mianji = 20;
      max_mianji = 50;
    }else if(index==3){
      min_mianji = 50;
      max_mianji = 100;
    }else if(index==4){
      min_mianji = 100;
      max_mianji = 500;
    }else if(index==5){
      min_mianji = 500;
    }
    var tiaojian  = this.data.tiaojian;
    tiaojian.min_mianji = min_mianji;
    tiaojian.max_mianji = max_mianji;
    this.setData({tiaojian:tiaojian,page:1,shuju:[]});
    this.getclass(1);
  },
  bindPickerChange3(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index3: e.detail.value,
      xianshi3: false
    })
    var fenleiid = this.data.fenleiid;
    console.log(fenleiid[e.detail.value]);
    var types = fenleiid[e.detail.value];
    var tiaojian = this.data.tiaojian;
    tiaojian.types = types;
    this.setData({tiaojian:tiaojian,page:1,shuju:[]});
    this.getclass(1);

  },
  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
  }, 
  //跳转到详情
  goinfo:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('id', id)
    wx.navigateTo({
      url: '/pages/details/details',
    })
  },
  //获取数据
  getclass: function (page){
    var tiaojian = this.data.tiaojian;
    var that = this;
    var openid = wx.getStorageSync('openid');
    tiaojian.openid = openid;
    tiaojian.page=page;
    console.log(tiaojian)
    // return;
    wx.request({
      url: that.data.getclassUrls,
      data: tiaojian,
      method:'post',
      success:function(res){
        var data = res.data;
        console.log(data);
        if(data.code==200){
          var fenlei = [];
          var fenleiid = [];
          fenlei.push('不限');
          fenleiid.push('');
          ea.each(data.data.types,function(i,v){
            fenlei.push(v.name);
            fenleiid.push(v.id);
          })
          that.setData({fenlei:fenlei,fenleiid:fenleiid});
          that.setData({page:page})
          if (page == 1) {
            var shuju = [];
          } else {
            var shuju = that.data.shuju;
          }
          if(data.data.data.length<5){
            that.setData({page:0});
            wx.showToast({
              title: '没有更多数据了',
              icon:'none'
            })
          }
          console.log(page);
          ea.each(data.data.data,function(i,v){
            shuju.push(v);
          })
          that.setData({ shuju: shuju });
          var city = wx.getStorageSync('city');
          var array = ['全'+city];
          ea.each(data.data.qu,function(i,v){
            array.push(v.area_name);
            if (data.data.newqu == v.area_name){
              that.setData({ index: i+1, xianshi:false});
            }
          })
          that.setData({array:array});
          that.setData({quanju:true})
          wx.hideLoading();
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var city = wx.getStorageSync('city');
    var tiaojian = this.data.tiaojian;
    var fenlei_type = wx.getStorageSync('fenlei_type');
    var qu = wx.getStorageSync('qus');
    if(qu){
      wx.setStorageSync('qus', '');
      tiaojian.qu = qu;
    }
    var youzhi = wx.getStorageSync('youzhi');
    //console.log(youzhi);
    if (youzhi==1){
      wx.setStorageSync('youzhi', 0);
    }
    tiaojian.youzhi=youzhi;
    if (fenlei_type){
      var navs = this.data.navs;
      var newnavs = [];
      ea.each(navs,function(i,v){
        if(v.id==fenlei_type){
          v.checked = true;
        }else{
          v.checked = false;
        }
        newnavs.push(v);
      })
      // var tiaojian = this.data.tiaojian;
      tiaojian.type = fenlei_type;
      that.setData({tiaojian:tiaojian,page:1,shuju:[],navs:newnavs});
      wx.setStorageSync('fenlei_type', '');
      this.getclass(1);
    }else{
      tiaojian.shi = city;
      that.setData({ tiaojian: tiaojian });
      this.getclass(1);
    }

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var page = that.data.page;
    if(page>0){
      page = page+1;
      this.getclass(page);
    }
  },



  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})