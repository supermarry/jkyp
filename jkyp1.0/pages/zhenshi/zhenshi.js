// pages/zhenshi/zhenshi.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:0,
    gouxuan:false,//  是否勾选
    fabuleibei: [ //发布类别
      {
       
        imgurl: '/assets/s1.png',
        name: "身份真实",
        gongneng: "依据真实身份发帖",
      },
      {
       
        imgurl: '/assets/s2.png',
        name: "上传实拍图片",
        gongneng: "上传与房源相关的真实图片"
      },
      {
        
        imgurl: '/assets/s3.png',
        name: "房源如实描述",
        gongneng: "房源是真实在租，位置及厅室等信息真实有效等"
      },
      {
   
        imgurl: '/assets/s4.png',
        name: "房租标价合理",
        gongneng: "标明实际租住价格，承诺不收额外费用"
      },
    ],


  },
  // 点击勾选状态
  change: function () {
    if (this.data.gouxuan == true) {     
      this.setData({
        gouxuan:false
      })
      console.log(111)
    } else {
      this.setData({
        gouxuan: true
      })
      console.log(222)
    }
  },
// 跳转至规则页面
  toguige:function(){
    wx.navigateTo({
      url: '../guize/guize',
    })
  },
  // 点击立即发布跳转至相应的页面
  tofabu:function(){
    if(this.data.gouxuan==true){
      if(this.data.id==0){
          wx.navigateTo({
            url: '../layout_shangpu/layout_shangpu',
          })
      }
      if (this.data.id == 1) {
        wx.navigateTo({
          url: '../layout_xiezilou/layout_xiezilou',
        })
      } if (this.data.id == 2) {
        wx.navigateTo({
          url: '../layout_changfang/layout_changfang',
        })
      } if (this.data.id == 3) {
        wx.navigateTo({
          url: '../layout_ershoufang/layout_ershoufang',
        })
      } if (this.data.id == 4) {
        wx.navigateTo({
          url: '../layout_qiuzu/layout_qiuzu',
        })
      }
    
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     let id = options.id
      this.setData({
        id:id
      })
      console.log(this.data.id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})