// pages/my_recent/my_recent.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page:1,
    limit:4,
    getlioulan:app.data.url+'api/api/getlioulan',
    quanju:false,
    lioulans:[
      {
        time:'',
        shuju:[]
      }
    ],
    // 搜索结果数组
    fangyuan: [
      {
        fimg: "/assets/f1.png",    //房源图片
        fname: "临街门面转让",      //房源标题
        fmianji: "1145",          //房源面积
        fdesc: "旺铺转让",     //房源描述
        flocation: '成华区建设路',   //房源位置区域
        fjiaotong: "建设路北3段3号",    //房源具体位置
        fbiaoqian: ["独立阳台", "配套齐全", "朝南"],   // 房源标签说明
        fprice: 28000
      },
      {
        fimg: "/assets/f1.png",    //房源图片
        fname: "临街门面转让",      //房源标题
        fmianji: "45",          //房源面积
        fdesc: "旺铺转让",     //房源描述
        flocation: '成华区建设路',   //房源位置区域
        fjiaotong: "建设路北3段3号",    //房源具体位置
        fbiaoqian: ["独立阳台", "配套齐全", "朝南"],   // 房源标签说明
        fprice: 28000
      },
      {
        fimg: "/assets/f1.png",    //房源图片
        fname: "临街门面转让",      //房源标题
        fmianji: "45",          //房源面积
        fdesc: "旺铺转让",     //房源描述
        flocation: '成华区建设路',   //房源位置区域
        fjiaotong: "建设路北3段3号",    //房源具体位置
        fbiaoqian: ["独立阳台", "配套齐全", "朝南"],   // 房源标签说明
        fprice: 28000
      },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
wx.showLoading({
  title: '加载中',
})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var page = that.data.page;
    var limit = that.data.limit;
    var lioulan = wx.getStorageSync('lioulan');
    console.log(lioulan);
    var openid = wx.getStorageSync('openid');
    if(!lioulan){
      return;
    }
    lioulan = lioulan.reverse();
    wx.request({
      url: that.data.getlioulan,
      data:{openid:openid,shuju:lioulan},
      success:function(res){
        var data = res.data;
        console.log(data);
        if(data.code==200){
          var lioulans = [];
          var shijian = ea.timeToarray();
          shijian = shijian.year+'-'+shijian.month+'-'+shijian.day;
          // console.log(data.data)
        
            ea.each(data.data,function(i,v){
              if(i==shijian){
                i = '今天';
              }
              lioulans.push({
                time:i,
                shuju:v
              });
            });
          console.log(lioulans);
          that.setData({ lioulans: lioulans})
          that.setData({quanju:true})
          wx.hideLoading();
        
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  tiaoout:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('id', id);
    wx.navigateTo({
      url: '/pages/details/details',
    })
  },
  quqiouzu:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('rent_id', id);
    console.log(id)
    wx.navigateTo({
      url: '/pages/qiuzu_detail/qiuzu_detail',
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})