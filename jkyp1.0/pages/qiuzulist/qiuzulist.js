// pages/qiuzulist/qiuzulist.js
var app =getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shi:'',
    type:'',
    qu:'',
    min:0,
    max:'',
    page:1,
    index:'',
    xianshi:true,
    index1: '',
    xianshi1: true,
    index2: '',
    xianshi2: true,
    getchuzulist:app.data.url+'api/api/chuzulist',

    //条件选择数组[地区，分类，面积，更多]
    // 1.1地区数组
    array: [
      '成都', '锦江区', '武侯区', '天府新区'
    ],
    arrayid: [11, 12, 13, 14],
    // 1.2分类数组
    array1: [
      "新房", "二手房"
    ],
    array1id: [],
    // 1.3面积数组
    array2: [
      "不限", "10m²-20m²", "20m²-30m²", "30m²-50m²", "50m²-100m²", "100m²-500m²", "500m²-1000m²","1000m²以上"
    ],
    qiouzuss:[
    
    ]
  },
  //
  bindPickerChange(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    var index = e.detail.value;
    var array = this.data.array;
    this.setData({ qu: array[index]});
    this.setData({
      index: e.detail.value,
      xianshi:false
    })
    this.setData({ qiouzuss:[]});
    this.setData({page:1});
    this.getchuzulists();
  },
  bindPickerChange1(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    var index = e.detail.value;
    var type = this.data.array1id[index];
    this.setData({type:type});
    this.setData({
      index1: e.detail.value,
      xianshi1: false
    })
    this.setData({page:1});
    this.setData({ qiouzuss:[]})
    this.getchuzulists();
  },
  bindPickerChange2(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    var index = e.detail.value;
    if(index==0){
      this.setData({min:0,max:''})
    }
    if(index==1){
      this.setData({ min: 10,max:20})
    }
    if (index == 2) {
      this.setData({ min: 20, max: 30})
    }
    if (index == 3) {
      this.setData({ min: 30, max: 40})
    }
    if (index == 4) {
      this.setData({ min: 50, max: 50})
    }
    if (index == 5) {
      this.setData({ min: 100, max: 500})
    }
    if (index == 6) {
      this.setData({ min: 500, max: 1000})
    }
    if (index == 7) {
      this.setData({ min: 1000, max: '' })
    }
    this.setData({
      index2: e.detail.value,
      xianshi2: false
    })
    this.setData({ qiouzuss:[]});
    this.setData({ page: 1 });
    this.getchuzulists();
  },
  goinfo:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('rent_id', id);
   wx.navigateTo({
     url: '/pages/qiuzu_detail/qiuzu_detail',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var city = wx.getStorageSync('city');
    var openid = wx.getStorageSync('openid');
    that.setData({ shi: city})
    this.getchuzulists();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  getchuzulists: function (){
    var that = this;
    var openid = wx.getStorageSync('openid');
    var page = that.data.page;
    if(page==0){
      return;
    }
    var shi = that.data.shi;
    var qu = that.data.qu;
    var type = that.data.type;
    var min = that.data.min;
    var max = that.data.max;
    console.log(max);
    // console.log(that.data.getchuzulist);
    wx.request({
      url: that.data.getchuzulist,
      data:{openid:openid,shi:shi,qu:qu,type:type,min:min,max:max,page:page},
      success:function(res){
        var data = res.data;
        console.log(data);
        var chen = data.data.chen;
        var shuju = data.data.data;
        var leixing = data.data.leixing;
        var array = [];
        ea.each(chen,function(i,v){
          array.push(v.area_name);
        })
        that.setData({array:array});
        console.log(shuju)
        var qiouzuss = that.data.qiouzuss;
        if (shuju.length<5){
          that.setData({page:0})
        }
        ea.each(shuju,function(i,v){
          var name = v.last_name;
          if(v.sex=='男'){
            name = name+'先生';
          }else{
            name = name + '女士';
          }
          qiouzuss.push({
            id:v.id,
            name:name,
            img: v.user.head_portrait,
            qu:v.qu,
            type:v.type,
            min_mianji: v.min_mianji,
            max_mianji: v.max_mianji,
            dir: v.dir,
            min_rmb: v.min_jiage,
            max_rmb: v.max_jiage,
            time: v.qiuzu_time
          })
        })
        that.setData({ qiouzuss: qiouzuss});
        var array1 = [];
        var array1id = [];
        console.log(leixing)
        ea.each(leixing,function(i,v){
          array1.push(v.name);
          array1id.push(v.id);
        })
        that.setData({ array1: array1})
        that.setData({ array1id: array1id })
      }
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var page = that.data.page;
    if(page==0){
      return;
    }
    page = page+1;
    that.setData({page:page});
    that.getchuzulists();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})