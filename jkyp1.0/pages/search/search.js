// pages/search/search.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
    getsousuourl:app.data.url+'api/newapi/getsousuo',
    shuju:[],
    tiaojian:'',
    // 搜索店铺 结果数组
    fangyuan: [
      {
        fimg: "/assets/f1.png",    //房源图片
        fname: "临街门面转让",      //房源标题
        fmianji: "1145",          //房源面积
        fdesc: "旺铺转让",     //房源描述
        flocation: '成华区建设路',   //房源位置区域
        fjiaotong: "建设路北3段3号",    //房源具体位置
        fbiaoqian: ["独立阳台", "配套齐全", "朝南"],   // 房源标签说明
        fprice: 28000
      },
      {
        fimg: "/assets/f1.png",         //房源图片
        fname: "临街门面转让",          //房源标题
        fmianji: "45",                //房源面积
        fdesc: "旺铺转让",            //房源描述
        flocation: '成华区建设路',    //房源位置区域
        fjiaotong: "建设路北3段3号",      //房源具体位置
        fbiaoqian: ["独立阳台", "配套齐全", "朝南"],  // 房源标签说明
        fprice: 28000
      },
    ],

  },
  //返回上一层
  backto:function(){
    var that = this;
    var tiaojian = that.data.tiaojian;
    if(!tiaojian){
      wx.showToast({
        title: '请输入搜索条件',
        icon:'none'
      })
      return;
    }
    var openid = wx.getStorageSync('openid');
    wx.request({
      url: that.data.getsousuourl,
      data:{openid:openid,tiaojian:tiaojian},
      method:'post',
      success:function(res){
        var data = res.data;
        if(data.code==200){
          var shuju = data.data;
          console.log(shuju);
          that.setData({shuju:shuju});
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },
  shousuo:function(e){
    var tiaojian = e.detail.value;
    this.setData({ tiaojian: tiaojian})
  },
  gorent:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('rent_id', id);
    wx.navigateTo({
      url: '/pages/qiuzu_detail/qiuzu_detail',
    })
  },
  goout:function(e){
    var id = e.currentTarget.dataset.id;
    wx.setStorageSync('id', id);
    wx.navigateTo({
      url: '/pages/details/details',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})