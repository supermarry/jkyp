// pages/my_layout/my_layout.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getuserout:app.data.url+'api/api/getuserout',
    deleteoutUrl:app.data.url+'api/api/deleteout',
    quanju:false,
    shuju:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
wx.showLoading({
  title: '加载中',
})
  },
  // 跳转至求租页面
  toqiuzu:function(){
    wx.navigateTo({
      url: '../my_layout1/my_layout1'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  ououtinfo:function(e){
console.log(e)
var id = e.currentTarget.dataset.id;
wx.setStorageSync('id', id);
wx.navigateTo({
  url: '/pages/details/details',
})
  },
  goout:function(e){
    var that  = this;
    var id = e.currentTarget.dataset.id;
    var shuju = that.data.shuju;
    var xuanzhong = '';
    ea.each(shuju,function(i,v){
      if(v.id==id){
        xuanzhong = v;
      }
    })
    if (xuanzhong.type == 2 && xuanzhong.out_type==2){
      wx.navigateTo({
        url: '/pages/layout_shangpu_sale/layout_shangpu_sale?id='+id,
      })
    } else if (xuanzhong.type == 2 && xuanzhong.out_type == 1){
      wx.navigateTo({
        url: '/pages/layout_shangpu_chuzu/layout_shangpu_chuzu?id=' + id,
      })
    } else if (xuanzhong.type == 2 && xuanzhong.out_type == 3){
      wx.navigateTo({
        url: '/pages/layout_shangpu_zhuanrang/layout_shangpu_zhuanrang?id=' + id,
      })
    } else if (xuanzhong.type == 1 && xuanzhong.out_type == 2){
      wx.navigateTo({
        url: '/pages/layout_ershoufang_sale/layout_ershoufang_sale?id=' + id,
      })
    } else if (xuanzhong.type == 1 && xuanzhong.out_type == 1) {
      wx.navigateTo({
        url: '/pages/layout_ershoufang_chuzu/layout_ershoufang_chuzu?id=' + id,
      })
    }else if (xuanzhong.type == 3 && xuanzhong.out_type == 2) {
      wx.navigateTo({
        url: '/pages/layout_changfang_sale/layout_changfang_sale?id=' + id,
      })
    } else if (xuanzhong.type == 3 && xuanzhong.out_type == 1) {
      wx.navigateTo({
        url: '/pages/layout_changfang_chuzu/layout_changfang_chuzu?id=' + id,
      })
    } else if (xuanzhong.type == 3 && xuanzhong.out_type == 3) {
      wx.navigateTo({
        url: '/pages/layout_changfang_zhuanzu/layout_changfang_zhuanzu?id=' + id,
      })
    }else if (xuanzhong.type == 4 && xuanzhong.out_type == 2) {
      wx.navigateTo({
        url: '/pages/layout_xiezilou_sale/layout_xiezilou_sale?id=' + id,
      })
    } else if (xuanzhong.type == 4 && xuanzhong.out_type == 1) {
      wx.navigateTo({
        url: '/pages/layout_xiezilou_chuzu/layout_xiezilou_chuzu?id=' + id,
      })
    } else if (xuanzhong.type == 4 && xuanzhong.out_type == 3) {
      wx.navigateTo({
        url: '/pages/layout_xiezilou_zhuanzu/layout_xiezilou_zhuanzu?id=' + id,
      })
    }
    console.log(xuanzhong);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var openid = wx.getStorageSync('openid');
    wx.request({
      url: that.data.getuserout,
      data:{openid:openid},
      success:function(res){
        var data = res.data;
        console.log(data);
        if(data.code==200){
          console.log(data.data)
          that.setData({ shuju:data.data});
          that.setData({quanju:true})
          wx.hideLoading();
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },
  shanchu:function(e){
    var that = this;
    var openid = wx.getStorageSync('openid');
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title:'提示',
      content:'是否确定删除?',
      showCancel:true,
      cancelText:'取消',
      confirmText:'确定',
      success:function(e){
if(e.confirm==true){
  wx.request({
    url: that.data.deleteoutUrl,
    data: { openid: openid, id: id },
    success: function (res) {
      var data = res.data;
      console.log(data)
      if (data.code == 200) {
        var shuju = that.data.shuju;
        var xin = [];
        ea.each(shuju, function (i, v) {
          if (v.id !== id) {
            xin.push(v);
          }
        })
        that.setData({ shuju: xin });
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    }
  })
}
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})