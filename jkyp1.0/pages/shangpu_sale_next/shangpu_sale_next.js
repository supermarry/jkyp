// pages/shangpu_sale_next/shangpu_sale_next.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addoutUrl: app.data.url + 'api/newapi/addout',
    changeouturl: app.data.url + 'api/newapi/changeout',
    shuju:'',
    address:'',    //商铺地址
    wuyefei:'',    //物业费
    shuifei:'',    // 水费
    dianfei:'',   // 电费
    title:'',    // 输入的标题
    miaosu:'',
    biaoqian: '',
    xuanzepeitao:[],
    peitao:[  //配套设施
      
    ],
    // 位置信息选择
    quyuxianshi:true,
    region: ['全部', '全部', '全部'],
    customItem: '全部',
  // 临街状态
    items: [
      { name: '1', value: '是' },
      { name: '0', value: '否', checked: 'true' },
    ],
    kongzhi:1,
  },

  /**
    * 添加标签
    */
  biaoqian1: function (e) {
    this.setData({ biaoqian1: e.detail.value });
    console.log(e.detail.value);
  },
  biaoqian2: function (e) {
    this.setData({ biaoqian2: e.detail.value });
    console.log(e.detail.value);
  },
  biaoqian3: function (e) {
    this.setData({ biaoqian3: e.detail.value });
    console.log(e.detail.value);
  },

  bindRegionChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
    var dir = e.detail.value;
    var shuju = this.data.shuju;
    shuju.sheng = dir[0];
    shuju.shi = dir[1];
    shuju.qu = dir[2];
    console.log(shuju);
    this.setData({shuju:shuju});
  },
  
  // 用户选择的配套设施,在点击提交的
  mypeitao:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    var peitao = that.data.peitao;
    var newpeitao = [];
    ea.each(peitao,function(i,v){
      if(v.id==id){
        if(v.checked==true){
          v.checked = false;
        }else{
          v.checked = true;
        }
      }
        newpeitao.push(v);
    })
    that.setData({peitao:newpeitao});
  },


  // 临街状态函数
  radioChange: function (e) {
    var shuju = this.data.shuju;
    shuju.near_jie = e.detail.value;
    console.log(shuju);
    this.setData({shuju:shuju});
    console.log('radio发生change事件，携带value值为：', e.detail.value)
  },
  //修改
  xiougai:function(){
    if (this.data.kongzhi == 0) {
      return;
    }
    this.setData({ kongzhi: 0 })
    var that = this;
    var shuju = that.data.shuju;
    var biaoqian = [];
    if (shuju.biaoqian1) {
      biaoqian.push(shuju.biaoqian1);
    }
    if (shuju.biaoqian2) {
      biaoqian.push(shuju.biaoqian2);
    }
    if (shuju.biaoqian3) {
      biaoqian.push(shuju.biaoqian3);
    }
    shuju.biaoqian = biaoqian;
    var easy_traffic = [];
    if (shuju.easy_traffic1) {
      easy_traffic.push(shuju.easy_traffic1);
    }
    if (shuju.easy_traffic2) {
      easy_traffic.push(shuju.easy_traffic2);
    }
    shuju.easy_traffic = easy_traffic;
    var peitaosesi = [];
    var peitao = that.data.peitao;
    ea.each(peitao, function (i, v) {
      if (v.checked == true) {
        peitaosesi.push(v.id);
      }
    })
    shuju.peitao = peitaosesi;
    var yan = ea.yanzhen(shuju, {
      imgs: '请选择图片',
      types: '请输入类型',
      louceng: '请输入类型',
      proportion: '请输入面积',
      zhuangtai: '请输入经营状态',
      hangye: '请输入经营行业',
      price: '请输入售价',
      expect_shouyi: '请输入预期收益',
      last_name: '请输入联系人姓氏',
      lxr_phone: '请输入联系人手机',
      sheng: '请选择所在区域',
      shi: '请选择所在区域',
      qu: '请选择所在区域',
      title: '标题必填',
      dir: '请填写描述',
      regin: '请输入详细地址'
    });
    if (yan) {
      wx.showToast({
        title: yan,
        icon: 'none'
      })
      that.setData({ kongzi: 1 })
      return;
    }
    var img = shuju.img;
    var imgs = shuju.imgs;
    var delimg = [];
    var newimg = [];
    ea.each(imgs, function (i, v) {
      if (img.indexOf(v) == -1) {
        newimg.push(v);
      }
    })
    ea.each(img, function (i, v) {
      if (imgs.indexOf(v) == -1) {
        delimg.push(v);
      }
    })
    shuju.delimg = delimg;
    shuju.newimg = newimg;
    shuju.openid = wx.getStorageSync('openid');
    ea.upfile(newimg, function (v) {
      shuju.newimg = v;
      wx.request({
        url: that.data.changeouturl,
        data: shuju,
        method: 'post',
        success: function (res) {
          var data = res.data;
          if (data.code == 200) {
            wx.showToast({
              title: data.msg,
              icon: 'success'
            })
            setTimeout(function () {
              wx.navigateBack({
                delta: 2
              })
            }, 1000)
          } else {
            that.setData({kongzhi:1})
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        }
      })
    })
  },
  // 提交函数
  confirmziliao:function(){
    if (this.data.kongzhi == 0) {
      return;
    }
    this.setData({ kongzhi: 0 })
    // 获取配套设施的所有配套；
    // var newpeitao = [];
    var that = this;
    var shuju = that.data.shuju;
    var newpeitao = [];
    var peitao = that.data.peitao;
    ea.each(peitao,function(i,v){
      if(v.checked==true){
        newpeitao.push(v.id);
      }
    })
    var yan = ea.yanzhen(shuju,{
      imgs:'请选择图片',
      types:'请输入类型',
      louceng:'请输入类型',
      proportion: '请输入面积',
      zhuangtai: '请输入经营状态',
      hangye: '请输入经营行业',
      price: '请输入售价',
      expect_shouyi: '请输入预期收益',
      last_name: '请输入联系人姓氏',
      lxr_phone: '请输入联系人手机',
      sheng:'请选择所在区域',
      shi:'请选择所在区域',
      qu: '请选择所在区域',
      title:'标题必填',
      dir: '请填写描述',
      regin: '请输入详细地址'
    });
    if(yan){
      wx.showToast({
        title: yan,
        icon:'none'
      })
      that.setData({kongzhi:1})
      return;
    }
    shuju.peitao = newpeitao;
    var biaoqian = [];
    if (shuju.biaoqian1){
      biaoqian.push(shuju.biaoqian1);
    }
    if (shuju.biaoqian2) {
      biaoqian.push(shuju.biaoqian2);
    }
    if (shuju.biaoqian3) {
      biaoqian.push(shuju.biaoqian3);
    }
    shuju.biaoqian = biaoqian;
    var easy_traffic = [];
    if (shuju.easy_traffic1){
      easy_traffic.push(shuju.easy_traffic1);
    }
    if (shuju.easy_traffic2) {
      easy_traffic.push(shuju.easy_traffic2);
    }
    shuju.easy_traffic = easy_traffic;
    ea.upfile(shuju.imgs,function(v){
      shuju.imgs = v;
      shuju.out_type = 2;
      var openid = wx.getStorageSync('openid');
      shuju.openid = openid;
      shuju.type = 2;
      console.log(shuju)
      wx.request({
        url: that.data.addoutUrl,
        method:'post',
        data:shuju,
        success:function(res){
          var data = res.data;
          if(data.code==200){
            wx.showToast({
              title: '提交成功',
              icon:'success'
            });
          setTimeout(function(){
            wx.switchTab({
              url: '/pages/layout/layout',
            })
          },1000);
          }else{
            that.setData({kongzhi:1})
            wx.showToast({
              title: data.msg,
              icon:'none'
            })
          }
        }
      })
    })
    // console.log(shuju.imgs);
  },
  // 获取页面所有input框中的值
  inputWacth:function(e){  
    let item = e.currentTarget.dataset.model;
  var shuju = this.data.shuju;
  shuju[item] = e.detail.value;
  console.log(shuju);
  this.setData({shuju:shuju});
    this.setData({
      [item]: e.detail.value
    });
    // console.log(this.data.address,this.data.wuyefei,this.data.shuifei,this.data.dianfei,this.data.title,this.data.miaosu.length)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var shuju = wx.getStorageSync('shuju');
    var zpeitao = wx.getStorageSync('peitao');
    console.log(zpeitao);
    var peitao = [];
    ea.each(zpeitao,function(i,v){
      peitao.push({id:v.id,name:v.title,checked:false});
    })
    this.setData({ peitao: peitao});
    if(shuju.peitao){
      var newpeitao = [];
      ea.each(peitao,function(i,v){
        if(shuju.peitao.indexOf(v.id)==-1){
          v.checked = false;
        }else{
          v.checked = true;
        }
        newpeitao.push(v);
      })
      this.setData({peitao:newpeitao})
    }
    if(shuju.id){
      var region = [];
      region.push(shuju.sheng);
      region.push(shuju.shi);
      region.push(shuju.qu);
      this.setData({ region: region});
      if (shuju.near_jie==1){
        var items = [
          { name: '1', value: '是', checked: true },
          { name: '0', value: '否', checked: false },
        ];
      }else{
        var items = [
          { name: '1', value: '是', checked: false },
          { name: '0', value: '否', checked: true },
        ];
      }
      this.setData({ items: items})
    }
    wx.setStorageSync('shuju', '');
    wx.setStorageSync('peitao', '');
    this.setData({shuju:shuju});
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})