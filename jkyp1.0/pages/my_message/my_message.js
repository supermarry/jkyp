// pages/my_message/my_message.js
var app =getApp();
var ea = require("../../utils/each.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    usermessge:app.data.url+'api/api/usermessge',
    messge:[

    ],
    page:1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var openid = wx.getStorageSync('openid');
    var that = this;
    var page = that.data.page;
    wx.request({
      url: that.data.usermessge,
      data:{openid:openid},
      success:function(res){
        var data = res.data;
        if(data.code==200){
          if(data.data.length<10){
            that.setData({page:0})
          }
          that.setData({messge:data.data});
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var openid = wx.getStorageSync('openid');
    var that = this;
    var page = that.data.page;
    console.log(page)
    if (page < 1) {
      return;
    }
    page = page+1;
    wx.request({
      url: that.data.usermessge,
      data: { page: page, openid: openid },
      success: function (res) {
        var data = res.data;
        var messge = that.data.messge;
        if(data.data.length<10){
          that.setData({page:0});
        }
        ea.each(data.data,function(i,v){
          messge.push(v);
        })
        that.setData({messge:messge})
      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})