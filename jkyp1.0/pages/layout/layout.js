// pages/layout/layout.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fabuleibei:[ //发布类别
      {
        id:0,
        imgurl:'/assets/shangpu.png',
        name:"商铺",
        gongneng:"出租、出售、转让",
      },
      {
        id: 1,
        imgurl: '/assets/fxizilou.png',
        name: "写字楼",
        gongneng: "出租、出售、转租"
      },
      {
        id: 2,
        imgurl: '/assets/fchangfang.png',
        name: "厂房土地仓库",
        gongneng: "出租、出售、转让"
      },
      {
        id: 3,
        imgurl: '/assets/fershoufang.png',
        name: "二手房",
        gongneng: "出租、出售"
      },
      {
        id: 4,
        imgurl: '/assets/fqiuzu.png',
        name: "求租",
        gongneng: "求租、求购房产、场地"
      },
    ]

  },
tozhenshi:function(e){
  let id = e.currentTarget.dataset.id
  wx.navigateTo({
    url: '../zhenshi/zhenshi?id='+id,
  })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})