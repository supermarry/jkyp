// pages/layout_xiezilou_chuzu.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addoutUrl: app.data.url + 'api/newapi/addout',
    changeouturl: app.data.url + 'api/newapi/changeout',
    shuju: { sex: '女' },
    getxzltypeurl: app.data.url + 'api/newapi/gettype',
    easy_traffic1: '',
    easy_traffic2: '',
    xzl_pare: app.data.url + 'api/classify/xzl_sale',
    backimg: "http://jikeyoupu.cybwnt.com/backgro.jpg", //背景图图片url地址
    // 男女状态
    items: [
      { name: 1, value: '男' },
      { name: 0, value: '女', checked: 'true' },
    ],
    keyishangchuang: true,
    region: ['全部', '全部', '全部'],
    xianshi: true,
    xianshi1: true,
    xzlleixing: ["纯写字楼", "商业综合体", "商务公寓"],  //写字楼类型
    xzlleixingid: [],  //写字楼类型id
    index: "",    //写字楼类型点击选择的第几项，通过index可以获取id
    xianshi: true,   //请选择控制 默认为true
    peitao: [  //配套设施
      {
        id: 0,
        name: "客梯",
        checked: 'false'
      },
      {
        id: 1,
        name: "货梯",
        checked: 'false'
      },
      {
        id: 2,
        name: "空调",
        checked: 'false'
      },
      {
        id: 3,
        name: "停车位",
        checked: 'false'
      }
    ],
    kongzi:1,
  },
  tomiaoshu: function () {
    wx.navigateTo({
      url: '../describe/describe',
    })
  },
  upload: function () {
    var imgs = this.data.shuju.imgs;
    if(imgs){
      wx.setStorageSync('imgs', imgs);
    }
    wx.navigateTo({
      url: '/pages/upimg/upimg',
    })
  },
  //修改
  xiougai: function () {
    if(this.data.kongzi==0){
      return;
    }
    this.setData({kongzi:0})
    var that = this;
    var shuju = that.data.shuju;
    var biaoqian = [];
    if (shuju.biaoqian1) {
      biaoqian.push(shuju.biaoqian1);
    }
    if (shuju.biaoqian2) {
      biaoqian.push(shuju.biaoqian2);
    }
    if (shuju.biaoqian3) {
      biaoqian.push(shuju.biaoqian3);
    }
    shuju.biaoqian = biaoqian;
    var easy_traffic = [];
    if (shuju.easy_traffic1) {
      easy_traffic.push(shuju.easy_traffic1);
    }
    if (shuju.easy_traffic2) {
      easy_traffic.push(shuju.easy_traffic2);
    }
    shuju.easy_traffic = easy_traffic;
    var peitaosesi = [];
    var peitao = that.data.peitao;
    ea.each(peitao, function (i, v) {
      if (v.checked == true) {
        peitaosesi.push(v.id);
      }
    })
    shuju.peitao = peitaosesi;
    var shu = ea.yanzhen(shuju, {
      imgs: "请选择图片",
      sheng: '请输入详细区域',
      shi: '请输入详细区域',
      qu: '请输入详细区域',
      regin: '请输入地址',
      proportion: '请输入面积',
      zujin: '请输入租金',
      // price: '请输入出售价格',
      types: '请输入类型',
      title: '请输入标题',
      dir: '请输入描述',
      last_name: '请输入联系人姓',
      lxr_phone: '请输入联系人手机'
    });
    if (shu) {
      wx.showToast({
        title: shu,
        icon: 'none'
      });
      that.setData({kongzi:1})
      return;
    }
    var img = shuju.img;
    var imgs = shuju.imgs;
    var delimg = [];
    var newimg = [];
    ea.each(imgs, function (i, v) {
      if (img.indexOf(v) == -1) {
        newimg.push(v);
      }
    })
    ea.each(img, function (i, v) {
      if (imgs.indexOf(v) == -1) {
        delimg.push(v);
      }
    })
    shuju.delimg = delimg;
    shuju.newimg = newimg;
    shuju.openid = wx.getStorageSync('openid');
    // console.log(shuju);
    // return;
    ea.upfile(newimg, function (v) {
      shuju.newimg = v;
      wx.request({
        url: that.data.changeouturl,
        method: 'post',
        data: shuju,
        success: function (res) {
          var data = res.data;
          if (data.code == 200) {
            wx.showToast({
              title: '修改成功'
            })
            setTimeout(function () {
              wx.navigateBack({
                delta: 1
              })
            }, 1000)
          } else {
            that.setData({kongzi:1})
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        }
      })
    })
  },
  //发布 
  fabu: function () {
    if(this.data.kongzi==1){
      var that = this;
      that.setData({kongzi:0})
      var shuju = that.data.shuju;
      shuju.type = 3;
      shuju.out_type = 1;
      var biaoqian = [];
      if (shuju.biaoqian1) {
        biaoqian.push(shuju.biaoqian1);
      }
      if (shuju.biaoqian2) {
        biaoqian.push(shuju.biaoqian2);
      }
      if (shuju.biaoqian3) {
        biaoqian.push(shuju.biaoqian3);
      }
      shuju.biaoqian = biaoqian;
      var easy_traffic = [];
      if (shuju.easy_traffic1) {
        easy_traffic.push(shuju.easy_traffic1);
      }
      if (shuju.easy_traffic2) {
        easy_traffic.push(shuju.easy_traffic2);
      }
      shuju.easy_traffic = easy_traffic;
      var peitaosesi = [];
      var peitao = that.data.peitao;
      ea.each(peitao, function (i, v) {
        if (v.checked == true) {
          peitaosesi.push(v.id);
        }
      })
      shuju.peitao = peitaosesi;
      var shu = ea.yanzhen(shuju, {
        imgs: "请选择图片",
        sheng: '请输入详细区域',
        shi: '请输入详细区域',
        qu: '请输入详细区域',
        regin: '请输入地址',
        proportion: '请输入面积',
        zujin: '请输入租金',
        // price: '请输入出售价格',
        types: '请输入类型',
        title: '请输入标题',
        dir: '请输入描述',
        last_name: '请输入联系人姓',
        lxr_phone: '请输入联系人手机'
      });
      if (shu) {
        wx.showToast({
          title: shu,
          icon: 'none'
        });
        that.setData({ kongzi: 1 })
        return;
      }
      shuju.openid = wx.getStorageSync('openid');
      ea.upfile(shuju.imgs, function (v) {
        shuju.imgs = v;
        shuju.openid = wx.getStorageSync('openid');
        wx.request({
          url: that.data.addoutUrl,
          method: 'post',
          data: shuju,
          success: function (res) {
            var data = res.data;
            if (data.code == 200) {
              wx.showToast({
                title: '发布成功'
              })
              setTimeout(function () {
                wx.switchTab({
                  url: '/pages/layout/layout',
                })
              }, 1000)
            } else {
              that.setData({kongzi:1});
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
            }
          }
        })
      })
    }else{
      return;
    }
  },
  radioChange: function (e) {
    var shuju = this.data.shuju;
    if (e.detail.value == 1) {
      shuju.sex = '男';
    } else {
      shuju.sex = '女';
    }
    console.log(shuju);
    this.setData({ shuju: shuju });
  },

  // 用户选择的配套设施,在点击提交的
  mypeitao: function (e) {
    var peitao = this.data.peitao;
    var newpeitao = [];
    var id = e.currentTarget.dataset.id;
    ea.each(peitao, function (i, v) {
      if (v.id == id) {
        if (v.checked == true) {
          v.checked = false;
        } else {
          v.checked = true;
        }
      }
      newpeitao.push(v);
    })
    this.setData({ peitao: peitao });

  },

  // 选择区域
  bindPickerChange(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value);
    var address = e.detail.value;
    var shuju = this.data.shuju;
    shuju.sheng = address[0];
    shuju.shi = address[1];
    shuju.qu = address[2];
    console.log(shuju);
    this.setData({ shuju: shuju, region: address });
    // this.setData({
    //   index: e.detail.value,
    //   xianshi: true,
    // })
  },

  /**
   * 添加楼层
   */
  addlouceng: function (e) {
    console.log(e.detail.value);
    this.setData({ louceng: e.detail.value })
  },

  addinput: function (e) {
    let item = e.currentTarget.dataset.model;
    var shuju = this.data.shuju;
    shuju[item] = e.detail.value;
    console.log(shuju)
    this.setData({ shuju: shuju });
  },
  addtypes: function (e) {
    console.log(e)
    var index = e.detail.value;
    var xzlleixingid = this.data.xzlleixingid;
    var shuju = this.data.shuju;
    shuju.types = xzlleixingid[index];
    console.log(shuju);
    this.setData({ shuju: shuju, index: index, xianshi1: false });
  },
  // 获取页面所有input框中的值
  inputWacth: function (e) {
    console.log(e)
    let item = e.currentTarget.dataset.model;
    this.setData({
      [item]: e.detail.value
    });
    console.log(item)
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setStorageSync('imgs', null);
    wx.setStorageSync('miaoshu', null)
    var that = this;
    var tiaojian = {};
    tiaojian.openid = wx.getStorageSync('openid');
    tiaojian.pid = 3;
    if(options.id){
      tiaojian.id = options.id;
    }
    wx.request({
      url: that.data.getxzltypeurl,
      data: tiaojian,
      method: 'post',
      success: function (res) {
        var data = res.data;
        if (data.code == 200) {
          console.log(data.data)
          var xzlleixing = [];
          var xzlleixingid = [];
          // console.log(data.data.data);
          ea.each(data.data.type, function (i, v) {
            xzlleixing.push(v.name);
            xzlleixingid.push(v.id)
          })
          that.setData({ xzlleixing: xzlleixing, xzlleixingid: xzlleixingid });
          var peitao = [];
          ea.each(data.data.peitao, function (i, v) {
            peitao.push({ id: v.id, name: v.title, checked: false });
            that.setData({ peitao: peitao })
          })
          if(data.data.data){
            var shuju = data.data.data;
            shuju.imgs = shuju.img;
            var region = [];
            region.push(shuju.sheng);
            region.push(shuju.shi);
            region.push(shuju.qu);
            shuju.biaoqian1 = shuju.biaoqian[0];
            shuju.biaoqian2 = shuju.biaoqian[1];
            shuju.biaoqian3 = shuju.biaoqian[2];
            wx.setStorageSync('miaoshu', shuju.dir);
            var xzlleixingid = that.data.xzlleixingid;
            ea.each(xzlleixingid,function(i,v){
              if(v==shuju.types){
                that.setData({ index: i, xianshi1:false});
              }
            })
            var peitao = that.data.peitao;
            var newpeitao = [];
            ea.each(peitao, function (i, v) {
              if (shuju.peitao.indexOf(v.id) == -1) {
                v.checked = false;
              } else {
                v.checked = true;
              }
              newpeitao.push(v);
            })
            if (shuju.sex == '男') {
              var items = [
                { name: 1, value: '男', checked: true },
                { name: 0, value: '女', checked: false },
              ];
            } else {
              var items = [
                { name: 1, value: '男', checked: false },
                { name: 0, value: '女', checked: true },
              ];
            }
            that.setData({ shuju: shuju, imgs: shuju.imgs, youtu: true, region: region,peitao:newpeitao,items:items});
          }
        } else {
          wx.showToast({
            title: data.msg,
            icon: 'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var imgs = wx.getStorageSync('imgs');
    var shuju = this.data.shuju;
    if (imgs) {
      this.setData({
        imgs: imgs,
        youtu: true,
        first_img: imgs[0],  //第一张显示的图片
        keyishangchuang: false,  //隐藏上传图片
      })
      shuju.imgs = imgs;
      // this.setData({ shuju: shuju })
      wx.setStorageSync('imgs', null);
    }
    var str = wx.getStorageSync('miaoshu');
    shuju.dir = str;
    console.log(shuju);
    this.setData({ shuju: shuju });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})