// pages/qiuzu/qiuzu.js
var app =getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getrenturl: app.data.url +'api/newapi/getrent',
    addrenturl:app.data.url+'api/newapi/addrent',
    changerenurl: app.data.url + 'api/newapi/cheagerent',
    id:'',
    kongzhi:1,
    shuju:{sex:'女'},
    region: ['全部', '全部', '全部'],   //期望区域
    customItem: '全部',    
    min_mianji:'',
    max_mianji:'',
    min_jiage:'',
    max_jiage:'',
    miaoshu:'',
    first_name:'',
    phone:'',
    leixingchecked:'',
    comstor_sex:'',
    leixing: [  //求租类型
      {
        id: 0,
        name: "新房",
        checked: 'false'
      },
      {
        id: 1,
        name: "二手房",
        checked: 'false'
      },
      ],
      // 性别
      sexs:[
        {
          id:0,
          sex:'先生',       
          cstyle:'btl',
          checked:false,
        },
        {
          id: 1,
          sex: '女士',
          cstyle: 'brb',
          checked: true
        },
      ],
      yewuguwen:13456988796      //业务顾问
      
  },
  // x类型选择
  mypeitao: function (e) {
    var that = this;
    var leixing = this.data.leixing;
    var id = e.currentTarget.dataset.id;
    var newleixing = [];
    ea.each(leixing,function(i,v){
      if(v.id==id){
        if(v.checked==true){
          var shuju = that.data.shuju;
          shuju.type = '';
          console.log(shuju);
          that.setData({ shuju: shuju });
          v.checked =false;
        }else{
          var shuju = that.data.shuju;
          shuju.type = v.id;
          console.log(shuju);
          that.setData({shuju:shuju});
          v.checked = true;
        }
      }else{
        v.checked =false;
      }
      newleixing.push(v);
    })
    this.setData({ leixing:newleixing})
  },
  //期望区域的选择
  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value);
    var array = e.detail.value;
    var shuju = this.data.shuju;
    shuju.sheng = array[0];
    shuju.shi = array[1];
    shuju.qu = array[2];
    console.log(shuju);
    this.setData({ shuju: shuju})
    this.setData({
      region: e.detail.value
    })
  },
  // 选择男女
  changebc:function(e){
    var index = e.currentTarget.dataset.index;
    console.log(index)
    var shuju = this.data.shuju;
    if(index==0){
      shuju.sex = '男';
    }else{
      shuju.sex = '女';
    }
    console.log(shuju);
    this.setData({shuju:shuju});
    var sexs = this.data.sexs;
    var newsexs = [];
    ea.each(sexs,function(i,v){
      if(v.id==index){
        v.checked = true;
      }else{
        v.checked = false;
      }
      newsexs.push(v);
    })
    this.setData({ sexs:newsexs}) 
  },
  // 获取页面所有input框中的值
  inputWacth: function (e) {  
    let item = e.currentTarget.dataset.model;
    var shuju = this.data.shuju;
    shuju[item] = e.detail.value;
    console.log(shuju);
    this.setData({shuju:shuju});
    this.setData({
      [item]: e.detail.value
    });
    // console.log(this.data.max_mianji, this.data.min_mianji)
  },
  update:function(){
    console.log(1111)
  },
  //修改
  xiougai:function(){
    var that = this;
    if(that.data.kongzhi==0){
      return;
    }
    that.setData({kongzhi:0})
    var shuju = that.data.shuju;
    shuju.openid = wx.getStorageSync('openid');
    wx.request({
      url: that.data.changerenurl,
      data:shuju,
      method:'post',
      success:function(res){
        var data = res.data;
        if(data.code==200){
          wx.showToast({
            title: '修改成功',
            icon: 'success'
          })
          setTimeout(function(){
            wx.navigateBack({
              delta:1,
            })
          },1000)
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })

  },
  //发布
  fabu:function(){
    var that = this;
    var shuju = this.data.shuju;
    shuju.rent_type = 1;
    var shu = ea.yanzhen(shuju,{
      type:'请输入求租的类型',
      sheng:'请输入期望区域',
      shi:'请输入期望区域',
      qu: '请输入期望区域',
      min_mianji:'请输入期望最小面积',
      max_mianji:'请输入期望最大面积',
      min_jiage:'请输入期望最低价格',
      max_jiage: '请输入期望最高价格',
      dir:'请输入描述',
      last_name:'请输入姓氏',
      phone:'请输入手机号以便好联系你'
      })
      if(shu){
        wx.showToast({
          title: shu,
          icon:'none'
        })
        return;
      }
    shuju.openid = wx.getStorageSync('openid');
    wx.request({
      url: that.data.addrenturl,
      data:shuju,
      method:'post',
      success:function(res){
        var data = res.data;
        if(data.code==200){
          wx.showToast({
            title: '发布成功'
          })
          setTimeout(function(){
            wx.switchTab({
              url: '/pages/layout/layout',
            })
          },1000)
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    var that = this;
    var id = '';
    if (options.id){
      var id = options.id;
      that.setData({ id: id });
    }
    this.getrentinfos(id);
  },
  getrentinfos:function(id){
    var openid = wx.getStorageSync('openid');
    var that = this;
    var tiaojian = {openid:openid};
    if(id){
      tiaojian.id=id;
    }
    console.log(tiaojian);
    wx.request({
      url: that.data.getrenturl,
      data:tiaojian,
      method:'post',
      success:function(res){
        var data = res.data;
        // console.log(data);
        if(data.code==200){
          console.log(data);
          if(data.data.data){
            that.setData({shuju:data.data.data});
            var region = [];
            if(data.data.data.sheng){
              region.push(data.data.data.sheng);
            }
            if (data.data.data.shi) {
              region.push(data.data.data.shi);
            }
            if (data.data.data.qu) {
              region.push(data.data.data.qu);
            }
            that.setData({region:region});
            if(data.data.data.sex=='男'){
              var sexs = [
                {
                  id: 0,
                  sex: '先生',
                  cstyle: 'btl',
                  checked: true,
                },
                {
                  id: 1,
                  sex: '女士',
                  cstyle: 'brb',
                  checked: false
                },
              ];
            }else{
              var sexs = [
                {
                  id: 0,
                  sex: '先生',
                  cstyle: 'btl',
                  checked: false,
                },
                {
                  id: 1,
                  sex: '女士',
                  cstyle: 'brb',
                  checked: true
                },
              ];
            }
            that.setData({sexs:sexs});
          }
          var leixing = [];
          ea.each(data.data.renttype, function (i, v) {
            if (data.data.data){
              if (data.data.data.type==v.id){
                leixing.push({ id: v.id, name: v.name, checked: true });
              }else{
                leixing.push({ id: v.id, name: v.name, checked: false });
              }
            }else{
              leixing.push({ id: v.id, name: v.name, checked: false });
            }
          })
          that.setData({ leixing: leixing })
          that.setData({ yewuguwen: data.data.yewuguwen.guwen_phone });
        }else{
          wx.showToast({
            title: data.msg,
            icon:'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // var that = this;
    // var openid = wx.getStorageSync('openid');
    // wx.request({
    //   url: that.data.getrenturl,
    //   data: { openid: openid },
    //   method: 'post',
    //   success: function (res) {
    //     var data = res.data;
    //     if (data.code == 200) {
    //       var leixing = [];
    //       ea.each(data.data.renttype, function (i, v) {
    //         leixing.push({ id: v.id, name: v.name, checked: false });
    //       })
    //       that.setData({ leixing: leixing })
    //       that.setData({ yewuguwen: data.data.yewuguwen.guwen_phone })
    //       console.log(data.data.yewuguwen.guwen_phone)
    //     } else {
    //       wx.showToast({
    //         title: data.msg,
    //         icon: 'none'
    //       })
    //     }
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})