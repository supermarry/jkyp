// pages/my_layout1/my_layout1.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
getuserrent:app.data.url+'api/api/getuserren',
deleterent:app.data.url+'api/api/deleterent',
shuju:[],
quanju:false,
src:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  gorent:function(e){
console.log(e)
var id = e.currentTarget.dataset.id;
    wx.setStorageSync('rent_id', id);
    wx.navigateTo({
      url: '/pages/qiuzu_detail/qiuzu_detail',
    })
  },
  tofangyuan:function(){
    wx.navigateTo({
      url: '../my_layout/my_layout',
     
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
var that = this;
var openid = wx.getStorageSync('openid');
wx.request({
  url: that.data.getuserrent,
  data:{openid:openid},
  success:function(res){
    var data = res.data;
    if(data.code==200){
      that.setData({shuju:data.data});
      console.log(data.data);
      that.setData({quanju:true})
       wx.hideLoading();
      
    }else{
      wx.showToast({
        title: data.msg,
        icon:'none'
      })
    }
  }
})
  },
  shanchu:function(e){
    var that = this;
var id = e.currentTarget.dataset.id;
var openid = wx.getStorageSync('openid');
wx.showModal({
  title: '提示',
  content: '你确定要删除吗？',
  showCancel:true,
  success:function(e){
    if(e.confirm==true){
      wx.request({
        url: that.data.deleterent,
        data: { openid: openid, id: id },
        success: function (res) {
          var data = res.data;
          if (data.code == 200) {
            var shuju = that.data.shuju;
            var xin = [];
            ea.each(shuju, function (i, v) {
              if (v.id !== id) {
                xin.push(v);
              }
            })
            that.setData({ shuju: xin });
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        }
      })
    }
  }
})
  },
  bianji:function(e){
    var that = this;
var id = e.currentTarget.dataset.id;
var shuju = that.data.shuju;
var xin = '';
ea.each(shuju,function(i,v){
  if(v.id==id){
    xin = v;
  }
})
    wx.navigateTo({
      url: '/pages/layout_qiuzu/layout_qiuzu?id=' + id,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})