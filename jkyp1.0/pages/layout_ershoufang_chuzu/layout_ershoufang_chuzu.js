// pages/layout_ershoufang_sale/layout_ershoufang_sale.js
var app = getApp();
var ea = require('../../utils/each.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getxzltypeurl: app.data.url + 'api/newapi/gettype',
    addouturl: app.data.url + 'api/newapi/addout',
    changeouturl:app.data.url+'api/newapi/changeout',
    shuju: { sex: '女' },
    ershoufang: app.data.url + 'api/Dengapi/ershoufang',
    imgs: [],  //上传的图片数组,将选择第一张作为图片展示
    first_img: '',
    youtu: false,    //默认为false,
    keyishangchuang: true,
    backimg: "http://jikeyoupu.cybwnt.com/backgro.jpg", //背景图图片url地址
    xianshi: true,
    region: ['全部', '全部', '全部'],  //选择的区域
    customItem: '全部',
    xianshi1: true,
    index: '',
    ershoufangleixing: ['普通住宅', ' 公寓', '别墅', '平房', '新里洋房', '老公房', '四合院', '排屋', '其他', '商住楼'],
    ershoufangleixingid: [],
    //产权类型选择
    cq_type: ['普通住宅', ' 公寓'],
    cq_typeid: [],
    cq_type_index: '',
    cq_type_xianshi: true,
    //装修
    zhuangxiou: ['毛坯房', '简单装修'],
    zhuangxiouid: [],
    zhuangxiou_index: '',
    zhuangxiou_xianshi: true,


    peitao: [  //配套设施
      {
        id: 0,
        name: "客梯",
        checked: 'false'
      },
      {
        id: 1,
        name: "货梯",
        checked: 'false'
      }
    ],
    // 男女状态
    items: [
      { name: '1', value: '男' },
      { name: '0', value: '女', checked: 'true' },
    ],
    kongzhi:1,
  },
  //区域的选择
  bindRegionChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    var shuju = this.data.shuju;
    var array = e.detail.value;
    shuju.sheng = array[0];
    shuju.shi = array[1];
    shuju.qu = array[2];
    console.log(shuju);
    this.setData({ shuju: shuju });
    this.setData({
      region: e.detail.value,
      xianshi: false
    })

  },
  //添加类型
  addtypes: function (e) {
    var index = e.detail.value;
    // var ershoufangleixing = this.data.ershoufangleixing;
    var ershoufangleixingid = this.data.ershoufangleixingid;
    var shuju = this.data.shuju;
    shuju.types = ershoufangleixingid[index];
    console.log(shuju);
    this.setData({ shuju: shuju, index: index, xianshi1: false });
  },
  //添加产权类型
  changecq_type: function (e) {
    var index = e.detail.value;
    var cq_typeid = this.data.cq_typeid;
    var shuju = this.data.shuju;
    shuju.cq_type = cq_typeid[index];
    console.log(shuju);
    this.setData({ shuju: shuju, cq_type_index: index, cq_type_xianshi: false });
  },
  //添加装修
  addzhuangxiou: function (e) {
    var index = e.detail.value;
    var zhuangxiouid = this.data.zhuangxiouid;
    var shuju = this.data.shuju;
    shuju.zhuangxiu = zhuangxiouid[index];
    console.log(shuju);
    this.setData({ shuju: shuju, zhuangxiou_index: index, zhuangxiou_xianshi: false });
  },
  //跳转到上传图片
  upload: function () {
    var img = this.data.imgs;
    if(img){
      wx.setStorageSync('imgs', img);
    }
    wx.navigateTo({
      url: '/pages/upimg/upimg',
    })
  },
  //获取数据
  inputWacth: function (e) {
    let item = e.currentTarget.dataset.model;
    var str = e.detail.value;
    var shuju = this.data.shuju;
    shuju[item] = str;
    console.log(shuju)
    this.setData({ shuju: shuju });
  },
  mypeitao: function (e) {
    var id = e.currentTarget.dataset.id;
    var peitao = this.data.peitao;
    var newpeitao = [];
    ea.each(peitao, function (i, v) {
      if (v.id == id) {
        if (v.checked == true) {
          v.checked = false;
        } else {
          v.checked = true;
        }
      }
      newpeitao.push(v)
    });
    this.setData({ peitao: newpeitao });
  },
  //确认修改
  xiougai:function(){
    if (this.data.kongzhi == 0) {
      return;
    }
    this.setData({ kongzhi: 0 })
    var that = this;
    var shuju = that.data.shuju;
    // console.log(shuju);
    var easy_traffic = [];
    if (shuju.easy_traffic1){
      easy_traffic.push(shuju.easy_traffic1);
    }
    if (shuju.easy_traffic2) {
      easy_traffic.push(shuju.easy_traffic2);
    }
    shuju.easy_traffic = easy_traffic;
    var biaoqian = [];
    if(shuju.biaoqian1){
      biaoqian.push(shuju.biaoqian1);
    }
    if (shuju.biaoqian2) {
      biaoqian.push(shuju.biaoqian2);
    }
    if (shuju.biaoqian3) {
      biaoqian.push(shuju.biaoqian3);
    }
    shuju.biaoqian = biaoqian;
    var peitao = that.data.peitao;
    var newpeitao = [];
    ea.each(peitao,function(i,v){
      if(v.checked==true){
        newpeitao.push(v.id);
      }
    })
    shuju.peitao = newpeitao;
    var shu = ea.yanzhen(shuju, {
      imgs: "请选择图片",
      sheng: '请输入详细区域',
      shi: '请输入详细区域',
      qu: '请输入详细区域',
      house_name: '请输入小区名称或详细地址',
      proportion: '请输入面积',
      // price: '请输入售价',
      hall: '请输入厅室情况',
      caoxiang: '请输入朝向',
      louceng: '请输入楼层',
      gongnuan: '请输入供暖情况',
      chanquan_year: '请输入产权年限',
      cq_type: '请选择产权类型',
      zhuangxiu: '请选择装修情况',
      zujin: '请输入租金',

      types: '请输入类型',
      title: '请输入标题',
      dir: '请输入描述',
      last_name: '请输入联系人姓',
      lxr_phone: '请输入联系人手机'
    });
    if (shu) {
      wx.showToast({
        title: shu,
        icon: 'none'
      });
      that.setData({ kongzi: 1 })
      return;
    }
    if(shuju.gongnuan=='有'){
      shuju.gongnuan =1;
    }else{
      shuju.gongnuan = 0;
    }
    var img = shuju.img;
    var imgs = shuju.imgs;
    var delimg = [];
    var newimg = [];
    ea.each(imgs,function(i,v){
      if(img.indexOf(v)==-1){
        newimg.push(v);
      }
    })
    ea.each(img,function(i,v){
      if(imgs.indexOf(v)==-1){
        delimg.push(v);
      }
    })
    shuju.delimg = delimg;
    shuju.openid = wx.getStorageSync('openid');
    // console.log(newimg);
    ea.upfile(newimg,function(v){
      shuju.newimg = v;
      wx.request({
        url: that.data.changeouturl,
        data:shuju,
        method:'post',
        success:function(res){
          var data = res.data;
          if(data.code==200){
            wx.showToast({
              title: data.msg,
              icon:'success'
            })
            setTimeout(function(){
              wx.navigateBack({
                delta:1
              })
            },1000)
          }else{
            that.setData({kongzhi:1})
            wx.showToast({
              title: data.msg,
              icon:'none'
            })
          }
        }
      })
    })
  },
  //确定发布
  fabu: function () {
    if (this.data.kongzhi == 0) {
      return;
    }
    this.setData({ kongzhi: 0 })
    var that = this;
    var shuju = that.data.shuju;
    shuju.type = 1;
    shuju.out_type = 1;
    var biaoqian = [];
    if (shuju.biaoqian1) {
      biaoqian.push(shuju.biaoqian1);
    }
    if (shuju.biaoqian2) {
      biaoqian.push(shuju.biaoqian2);
    }
    if (shuju.biaoqian3) {
      biaoqian.push(shuju.biaoqian3);
    }
    shuju.biaoqian = biaoqian;
    var easy_traffic = [];
    if (shuju.easy_traffic1) {
      easy_traffic.push(shuju.easy_traffic1);
    }
    if (shuju.easy_traffic2) {
      easy_traffic.push(shuju.easy_traffic2);
    }
    shuju.easy_traffic = easy_traffic;
    var peitaosesi = [];
    var peitao = that.data.peitao;
    ea.each(peitao, function (i, v) {
      if (v.checked == true) {
        peitaosesi.push(v.id);
      }
    })
    shuju.peitao = peitaosesi;
    var shu = ea.yanzhen(shuju, {
      imgs: "请选择图片",
      sheng: '请输入详细区域',
      shi: '请输入详细区域',
      qu: '请输入详细区域',
      house_name: '请输入小区名称或详细地址',
      proportion: '请输入面积',
      // price: '请输入售价',
      hall: '请输入厅室情况',
      caoxiang: '请输入朝向',
      louceng: '请输入楼层',
      gongnuan: '请输入供暖情况',
      chanquan_year: '请输入产权年限',
      cq_type: '请选择产权类型',
      zhuangxiu: '请选择装修情况',
      zujin: '请输入租金',

      types: '请输入类型',
      title: '请输入标题',
      dir: '请输入描述',
      last_name: '请输入联系人姓',
      lxr_phone: '请输入联系人手机'
    });
    if (shu) {
      wx.showToast({
        title: shu,
        icon: 'none'
      });
      that.setData({ kongzi: 1 })
      return;
    }
    shuju.openid = wx.getStorageSync('openid');
    if (shuju.gongnuan == '有') {
      shuju.gongnuan = 1;
    } else {
      shuju.gongnuan = 0;
    }
    ea.upfile(shuju.imgs, function (v) {
      shuju.imgs = v;
      shuju.openid = wx.getStorageSync('openid');
      wx.request({
        url: that.data.addouturl,
        method: 'post',
        data: shuju,
        success: function (res) {
          var data = res.data;
          if (data.code == 200) {
            wx.showToast({
              title: '发布成功'
            })
            setTimeout(function () {
              wx.switchTab({
                url: '/pages/layout/layout',
              })
            }, 1000)
          } else {
            that.setData({kongzhi:1})
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        }
      })
    })
  },
  adddir: function (e) {
    var str = e.detail.value;
    var shuju = this.data.shuju;
    shuju.dir = str;
    console.log(shuju);
    this.setData({ shuju: shuju });
  },
  //获取性别
  radioChange: function (e) {
    console.log(e.detail.value);
    var shuju = this.data.shuju;
    if (e.detail.value == 1) {
      shuju.sex = '男';
    } else {
      shuju.sex = '女';
    }
    console.log(shuju);
    this.setData({ shuju: shuju });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setStorageSync('imgs', null);
    wx.setStorageSync('miaoshu', null);
    var that = this;
    var tiaojian = {};
    tiaojian.openid = wx.getStorageSync('openid');
    tiaojian.pid    = 1;
    if(options.id){
      tiaojian.id = options.id;
    }
    console.log(tiaojian);
    wx.request({
      url: that.data.getxzltypeurl,
      data: tiaojian,
      method: 'post',
      success: function (res) {
        var data = res.data;
        if (data.code == 200) {
          console.log(data.data);
          var ershoufangleixing = [];
          var ershoufangleixingid = [];
          ea.each(data.data.type, function (i, v) {
            ershoufangleixing.push(v.name);
            ershoufangleixingid.push(v.id);
          })
          that.setData({ ershoufangleixing: ershoufangleixing, ershoufangleixingid: ershoufangleixingid });
          var peitao = [];
          ea.each(data.data.peitao, function (i, v) {
            peitao.push({ id: v.id, name: v.title, checked: false })
          });
          that.setData({ peitao: peitao })
          var cq_type = [];
          var cq_typeid = [];
          ea.each(data.data.cq, function (i, v) {
            cq_type.push(v.name);
            cq_typeid.push(v.id);
          })
          that.setData({ cq_type: cq_type, cq_typeid: cq_typeid });
          var zhuangxiou = [];
          var zhuangxiouid = [];
          ea.each(data.data.zhuangxiou, function (i, v) {
            zhuangxiou.push(v.name);
            zhuangxiouid.push(v.id)
          })
          that.setData({ zhuangxiou: zhuangxiou, zhuangxiouid: zhuangxiouid });
          if(data.data.data){
            var region = [];
            var shuju = data.data.data;
            console.log(shuju);
            if(shuju.img){
              shuju.imgs = shuju.img;
            }
            region.push(shuju.sheng);
            region.push(shuju.shi);
            region.push(shuju.qu);
            shuju.easy_traffic1 = shuju.easy_traffic[0];
            shuju.easy_traffic2 = shuju.easy_traffic[1];
            shuju.biaoqian1     = shuju.biaoqian[0];
            shuju.biaoqian2 = shuju.biaoqian[1];
            shuju.biaoqian3 = shuju.biaoqian[2];
            if(shuju.gongnuan==1){
              shuju.gongnuan = '有';
            }else{
              shuju.gongnuan = '无';
            }
            that.setData({ shuju: shuju, region: region, xianshi:false,});
            var ershoufangleixingid = that.data.ershoufangleixingid;
            ea.each(ershoufangleixingid,function(i,v){
              if(v==shuju.types){
                that.setData({ index: i, xianshi1:false});
              }
            })
            var cq_typeid = that.data.cq_typeid;
            ea.each(cq_typeid,function(i,v){
              if (v == shuju.cq_type){
                that.setData({ cq_type_index:i,cq_type_xianshi:false})
              }
            })
            var zhuangxiouid = that.data.zhuangxiouid;
            ea.each(zhuangxiouid,function(i,v){
              if (v == shuju.zhuangxiu){
                that.setData({ zhuangxiou_index: i, zhuangxiou_xianshi:false})
              }
            })
            if(shuju.sex=='男'){
              var items = [
                { name: '1', value: '男',checked:true },
                { name: '0', value: '女', checked: false },
              ];
            }else{
              var items = [
                { name: '1', value: '男', checked:false},
                { name: '0', value: '女', checked: true },
              ];
            }
            that.setData({items:items});
            var peitao = that.data.peitao;
            var newpeitao = [];
            ea.each(peitao,function(i,v){
              if(shuju.peitao.indexOf(v.id)==-1){
                v.checked = false;
              }else{
                v.checked = true;
              }
              newpeitao.push(v);
            })
            that.setData({ peitao: newpeitao});
            that.setData({ imgs: shuju.img, youtu:true,});
          }
        } else {
          wx.showToast({
            title: data.msg,
            icon: 'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var imgs = wx.getStorageSync('imgs');
    var shuju = this.data.shuju;
    if (imgs) {
      this.setData({
        imgs: imgs,
        youtu: true,
        first_img: imgs[0],  //第一张显示的图片
        keyishangchuang: false,  //隐藏上传图片
      })
      shuju.imgs = imgs;
      // this.setData({ shuju: shuju })
      wx.setStorageSync('imgs', null);
    }
    // console.log(shuju);
    this.setData({ shuju: shuju });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})