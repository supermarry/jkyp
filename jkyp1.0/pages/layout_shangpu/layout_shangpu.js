// pages/layout_shangpu/layout_shangpu.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    leixing: [   // 类型选择
      {
        id:0,
        name:"商铺出售"
       },
      {
        id: 1,
        name: "商铺出租"
      },
      {
        id: 2,
        name: "生意转让"
      },
    ]

  },
  toshangpu:function(e){
   let id = e.currentTarget.dataset.id
    if(id==0){
      wx.navigateTo({
        url: '../layout_shangpu_sale/layout_shangpu_sale',  //商铺出售
      })
    
    }
    if (id == 1) {
      wx.navigateTo({
        url: '../layout_shangpu_chuzu/layout_shangpu_chuzu', //商铺出租
      })

    }
    if (id == 2) {
      wx.navigateTo({
        url: '../layout_shangpu_zhuanrang/layout_shangpu_zhuanrang', //生意转让
      })
   
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})