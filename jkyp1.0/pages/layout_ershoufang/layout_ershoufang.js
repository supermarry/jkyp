// pages/layout_ershoufang/layout_ershoufang.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    leixing: [   // 类型选择
      {
        id: 0,
        name: "二手房出售"
      },
      {
        id: 1,
        name: "二手房出租"
      },
     
    ]
  },
  toershoufang: function (e) {
    let id = e.currentTarget.dataset.id
    if (id == 0) {
      wx.navigateTo({
        url: '../layout_ershoufang_sale/layout_ershoufang_sale',  //出售
      })

    }
    if (id == 1) {
      wx.navigateTo({
        url: '../layout_ershoufang_chuzu/layout_ershoufang_chuzu', //出租
      })
    }
  
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})