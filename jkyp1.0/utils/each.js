var qqMap = require('./qqmap-wx-jssdk.min.js');
var app = getApp();
var gettokenurl = app.data.url +'api/api/getQiniuToken';
//循环数组和json对象
function each(data, func) {
  if(Array.isArray(data)){
    for (var a = 0; a < data.length; a++) {
      func(a, data[a])
    }
  }else{
    var r = '';
    for(r in data){
      func(r,data[r]);
    }
  }
}
//获取当前的地址的详情
function getLocation(func){
  var data = [];
  wx.getLocation({
    type: 'wgs84',
    success: function (res) {
      // console.log(res)
      var latitude = res.latitude;
      var longitude = res.longitude;
      var app = getApp();
      var test = new qqMap({
        key: app.data.qqmapkey
      });
      test.reverseGeocoder({
        location: {
          latitude: latitude,
          longitude: longitude
        },
        success: function (ress) {
          // console.log(ress);
          var result = ress.result;
          // console.log(result);
          var data = { address: result.address, guo: result.address_component.nation, shen: result.address_component.province, xian: result.address_component.district, shi: result.address_component.city };
          if (func) {
            func(data)
          } else {
            console.log(data);
          }
        }
      })
    },
  })
}
//获取当前的时间数组
function timeToarray(time=null){
  if(!time){
    time = new Date();
  }
  var year = time.getFullYear();
  var month = time.getMonth()+1;
  if(month<10){
    month = '0'+month;
  }
  var day   = time.getDate();
  if(day<10){
    day = '0'+day;
  }
  return {year:year,month:month,day:day};
}
function yanzhen(data,array){
  var a = 0;
  var str = '';
  this.each(array,function(i,v){
    if(data[i]||data[i]==0){
      
    }else{
      a = 1;
      if (!str) {
        str = v;
      }
    }
  })
  if(a==1){
return str;
  }else{
    return 0;
  }
  
}
//获取数据的经纬度
function strToJingWei(str,func){
  var app = getApp();
  var jingwei = new qqMap({
    key:app.data.qqmapkey
  });
  jingwei.geocoder({
    address:str,
    success:function(res){
        if(res.status==0){
          var lat = res.result.location.lat;
          var lng = res.result.location.lng;
          var data = { latitude: lat, longitude :lng}
          func(data);
        }else{
          console.log('获取失败');
        }
    }
  })
}
//上传文件到七牛云
function upfile(src = [], func, change_name = true, name = []){
  var that  = this;
  wx.request({
    url: gettokenurl,
    method:'post',
    data:{},
    success:function(res){
      var data = res.data;
      if(data.code==200){
        if(src.length>0){
          var uptoken = data.data;
          var names = [];
          var times = '';
          var newname = '';
          for (var a = 0; a < src.length; a++) {
            if (change_name || name.length < 1) {
              times = (new Date()).getTime() + Math.floor(Math.random() * 10000);
              newname = src[a].split('.').pop();
              newname = times + '.' + newname;
            } else {
              if (name[a]) {
                newname = name[a];
              } else {
                newname = name[0];
              }
            }
            wx.uploadFile({
              url: 'https://upload.qiniup.com',
              filePath: src[a],
              name: 'file',
              formData: { token: uptoken, key: newname },
              success: function (res) {
                if (res.statusCode == 200) {
                  console.log('上传成功')
                } else {
                  console.log('上传失败');
                }
              }
            })
            names.push(newname);
          }
        }else{
          var names = [];
        }
        func(names)
      }else{
        wx.showToast({
          title: '获取上传凭证失败',
          icon:'none'
        })
      }
    }
  })
}
module.exports={
  each: each,
  getLocation: getLocation,
  strToJingWei: strToJingWei,
  timeToarray: timeToarray,
  upfile: upfile,
  yanzhen: yanzhen
}