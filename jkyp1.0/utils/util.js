const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
function getLocation(func){
  var qqMap = require('./qqmap-wx-jssdk.min.js');
  var data = [];
  wx.getLocation({
    type: 'wgs84',  
    success: function(res) {
      // console.log(res)
      var latitude = res.latitude;
      var longitude = res.longitude;
      var app = getApp();
      var test = new qqMap({
        key: app.data.qqmapkey
      });
      test.reverseGeocoder({
        location: {
          latitude: latitude,
          longitude: longitude
        },
        success:function(ress){
          // console.log(ress);
          var result = ress.result;
          // console.log(result);
          var data = { address: result.address, guo: result.address_component.nation, shen: result.address_component.province, xian: result.address_component.district, shi: result.address_component.city};
          if(func){
            func(data)
          }else{
            console.log(data);
          }
        }
      })
    },
  })
}
function each(data,func){
if(Array.isArray(data)){
    for(var a=0;a<data.length;a++){
      func(a,data[a]);
    }
  }else{
    var a = '';
    for(a in data){
      func(a,data[a]);
    }
  }
}

module.exports = {
  formatTime: formatTime,
  getLocation: getLocation
}
